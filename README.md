DragonCharts Readme
==========================

Este modulo consiste en un sistema que procesa los datos del sistema GNSS y los envía por diferentes canales para su visualización.

El canal principal que utiliza es DBUS, por el cual se podrían suscribir difierentes procesos que necesitan acceder a los datos que se están generando.

Un módulo, el DataWork obtiene los datos desde la base de datos del Collector (RethinkkDB) y los procesa para enviarlos. 

Otro módulo, el de interfaz gráfica adquiere los datos y los visualiza. Se ocupa para esto PyQt5 y PyqtGraph.
