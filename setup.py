#from distutils.core import setup, Extension
from setuptools import setup

setup(name='dragoncharts',
      version='0.1',
      description='Processing and Visualize data from Collector',
      url='http://gitlab.csn.uchile.cl/dpineda/dragonchart',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=[],
      zip_safe=False)
