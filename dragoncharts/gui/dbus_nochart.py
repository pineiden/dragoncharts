import sys
from datetime import datetime, timedelta

from PyQt5.QtCore import QObject, pyqtSignal, QSharedMemory
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import pyqtSlot, Q_CLASSINFO
from PyQt5 import QtCore, QtDBus
from networktools.ipc.dbus import GatherDBusAdaptor, GatherDBusInterface, GatherSignal
from multiprocessing import Manager, Queue

from quamash import QEventLoop, QThreadExecutor
from tasktools.taskloop import coromask, renew, simple_fargs, renew_quamash

#from dbus.mainloop.pyqt5 import DBusQtMainLoop

import time
import copy
import queue
import asyncio
import functools

from datetime import datetime as dt

import rethinkdb as rdb
import simplejson as json
# Async Socket

from networktools.colorprint import gprint, bprint, rprint
from networktools.library import my_random_string
from networktools.dbs.rethinkdb import RethinkDB
from networktools.time import get_datetime_di
from gnsocket.gn_socket import GNCSocket

#from .manager import QTManager
from .manager import QTManager


#D-bus chapter
service='csn.atlas.gather'
interface='%s.interface'%service

class GatherDBusAdaptor(QtDBus.QDBusAbstractAdaptor):
    Q_CLASSINFO("D-Bus Interface", interface)
    Q_CLASSINFO("D-Bus Introspection", ''
        '  <interface name="csn.atlas.gather.interface">\n'
        '	 <signal name="message">\n'
                '	   <arg type="s" name="data"/>\n'
        '	 </signal>\n'
        '  </interface>\n'
        '')	
    message = pyqtSignal(str)
    def __init__(self, parent):
        super().__init__(parent)
        self.setAutoRelaySignals(True)
        print("Abstract adaptor")

class GatherDBusInterface(QtDBus.QDBusAbstractInterface):
    message=pyqtSignal(str)
    def __init__(self, service, path, interface, connection, parent=None):
        super().__init__(service, path, interface, connection, parent)
        print("Gather DBus Adaptor ok")

class GatherSignal(QObject):
    signal=pyqtSignal(str)
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.path="/data"
        if 'path' in kwargs.keys():
            self.path=kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service=kwargs['service']
        self.interface="%s.interface" %self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
                self.variable=kwargs['variable']
        GatherDBusAdaptor(self)
        self.connection=QtDBus.QDBusConnection.sessionBus()
        self.connection.registerObject(self.path, self)
        self.connection.registerService(self.service)
        self.iface=GatherDBusInterface(self.service,
                                       self.path,
                                       self.interface,
                                       self.connection, self)
        self.connect(self.send)
    def message(self, msg):
        self.signal.emit(msg)
    @pyqtSlot(str)
    def send(self, msg):
        #data=msg#json.dumps(msg)
        print("send to dbus %s" %msg)
        #self.emit([data])
        self.emit(msg)
    def emit(self, data):
        msg = QtDBus.QDBusMessage.createSignal(self.path, self.interface, self.variable)
        msg << data
        print(msg)
        self.connection.send(msg)
    def connect(self, method):
        self.signal.connect(method)

class GuiNOCHART(QMainWindow, QTManager):
    signal=pyqtSignal(str)
    message=pyqtSignal(dict)
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.path="/data"
        if 'path' in kwargs.keys():
            self.path=kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service=kwargs['service']
        self.interface="%s.interface" %self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
            self.variable=kwargs['variable']
        if 'size' in kwargs.keys():
            self.size=kwargs['size']
        if 'nen' in kwargs.keys():
            self.nen=kwargs['nen']
        self.chart_type='sin_chart'
        if 'chart' in kwargs.keys():
            self.chart_type=kwargs['chart']
        if 'group' in kwargs.keys():
            self.group=kwargs['group']
        if 'queue_set' in kwargs.keys():
            self.queue_set=kwargs['queue_set']
        if 'rdb_address' in kwargs.keys():
            self.rethinkdb_address=kwargs['rdb_address']
        if 'rdb_dbname' in kwargs.keys():
            self.rethinkdb_dbname=kwargs['rdb_dbname']
        if 'queue_list' in kwargs.keys():
            self.queue_list = kwargs['queue_list']
        self.idg = 'gui'
        if 'id_gui' in kwargs.keys():
            self.idg = kwargs["id_gui"]
        if 'type_bus' in kwargs.keys():
            self.type_bus=kwargs['type_bus']
        # get set ov variables to control map gui
        self.read_queue_map = kwargs.get('rqm', Queue())
        self.write_queue_map = kwargs.get('wqm', Queue())
        self.map_status = kwargs.get('map_status', 'OFF')
        # OK
        self.nsta=len(self.group)
        self.data_queue=queue.Queue(maxsize=6)
        self.title='DragonCharts GNSS Real Time System'
        self.left=10
        self.top=10
        self.width=640
        self.height=480
        self.stations = {}
        self.assign_stations = {}
        self.process_group = {}
        self.position = {}
        self.guide = {}
        self.selected_sta=[]
        self.chart_flag=False
        self.status_idchart=None
        self.di=rdb.iso8601(get_datetime_di(delta=30))
        self.last_time={sta:self.di for sta in self.group}
        self.patrol=GatherSignal(self)
        self.new_datetime = datetime.now()+timedelta(minutes=1)
        # create signal to channel coord_ref
        signal_opts={'service':'csn.gui.coord_ref'}
        self.signal_ref = GatherSignal(**signal_opts)
        #self.up_gui()
        self.initUI()

    def setSignal(self, signal):
        #print("Signal %s" %signal)
        self.message=signal

    def up_gui(self):
        main_widget=QWidget()
        layout=QVBoxLayout(main_widget)
        self.text=QPlainTextEdit()
        self.setCentralWidget(self.text)
        self.button=QPushButton('Print Hola')
        self.button.clicked.connect(self.printhola)
        layout.addWidget(self.button)
        layout.addWidget(self.text)
        self.setCentralWidget(main_widget)
        self.signal.connect(self.print_log)
        self.connect_dbus()
        self.send_init()
        self.show()

    def initUI(self):
        self.main_widget=QWidget()
        # create button
        button=QPushButton("PyQT5 Button")
        button.setToolTip("Este es un botón de ejemplo")
        button.clicked.connect(self.on_click)
        # create button
        button_close=QPushButton("Cerrar")
        button_close.setToolTip("Cerrar Interfaz")
        button_close.clicked.connect(self.cerrar)
        #create textbox
        self.textbox = QLineEdit()
        self.textbox.resize(280,40)
        wn_options={'direction':2}
        args=[QBoxLayout.TopToBottom]
        idl=self.create_layout(None, *args,  **wn_options)
        vbox = self.layouts[idl]['instance']
        vbox.addWidget(button_close)
        vbox.addWidget(button)
        vbox.addWidget(self.textbox)
        ts_args=[]
        ts_options={}
        idts=self.create_tabset(idl, *ts_args, **ts_options)
        #Tab for first chart:>
        t1_args=[QBoxLayout.LeftToRight]
        t1_options={'name':'Gráficos 1'}
        idt1=self.create_tab(idts, *t1_args, **t1_options)
        self.idt1=idt1
        #Tab for log and stations log:>
        t2_args=[QBoxLayout.LeftToRight]
        t2_options={'name':'Log'}
        idt2=self.create_tab(idts, *t2_args, **t2_options)
        #Tab for data log:>
        t3_args=[QBoxLayout.LeftToRight]
        t3_options={'name':'Data Log'}
        idt3=self.create_tab(idts, *t3_args, **t3_options)
        # Tab for status
        """
        t4_args=[QBoxLayout.LeftToRight]
        t4_options={
            'name':'Status',
            'type_widget':'gnss_status',
            'stationsXcolumn':30,
            'lb':25,
            'lm':25
        }
        idt4=self.create_tab(idts, *t4_args, **t4_options)
        self.status_tab = idt4
        """
        #Text for Log:>
        text_args=[]
        text_options={}
        idtext=self.create_text(idt2, *text_args, **text_options)
        #Text for data log:>
        text_args2=[]
        text_options2={}
        idtext2=self.create_text(idt3, *text_args2, **text_options2)
        self.log_text=idtext
        self.log_data=idtext2
        self.text=self.log_data
        #  Set main window
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage("Mensaje en Status Bar")
        #show
        self.connect_dbus()
        self.send_init()
        self.show()


    def connect_dbus(self):
        #call adaptor for this channel
        GatherDBusAdaptor(self)
        #create connection
        self.connection=QtDBus.QDBusConnection.sessionBus()
        objectReg=self.connection.objectRegisteredAt(self.path)
        self.connection.unregisterObject(self.path)
        "=============="
        regObj=self.connection.registerObject(self.path, self.interface, self)# register path, object
        regSess=self.connection.registerService(self.service)# register service
        #call DBUS interface with my service, path, interface assigned to this object
        self.server=GatherDBusInterface(
                self.service,
                self.path,
                self.interface,
                self.connection,
                self)
        #connect dbus connection to slot
        self.connection.connect(
                self.service,
                self.path,
                self.interface,
                self.variable,
                self.update_data)#(service, path, interface, variable, slot_fn)
        rprint(["service","path","interface","variable"])
        bprint([self.service, self.path, self.interface, self.variable])
        if not QtDBus.QDBusConnection.sessionBus().isConnected():#check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" %self.service)
        print("Interface")
        print(self.server)
        print(self.server.message)
        #connect interface message signal to slot
        #self.server.message.connect(self.update_data)
        print("Server message %s" %self.server.message )
        #send a message
        #self.message.emit({"hola":"test message"})
    @pyqtSlot()
    def printhola(self):
        self.signal.emit("Hola\n")
        self.patrol.message({"msg":"desde gui"})

    """
    @pyqtSlot(list)
    def update_data(self, data):
        print("Data received from dbus %s" %data)
        self.signal.emit(json.dumps(data))

    """

    @pyqtSlot(str)
    def update_data(self, data):
        #bprint(data)
        new_data = json.loads(data)
        #rprint(type(new_data))
        msg=new_data
        command=msg['command']
        data=msg['data']
        #self.print_data(new_data)
        now = datetime.now()
        if command=='station':
            self.stations.update(data)
            station=tuple(data.items())
            self.guide.update({station[0][1]['code']:station[0][0]})
            station_ids=list(msg.keys())[0]
            #self.print_log(json.dumps(data))
        elif command=='position':
            self.position.update(data)
            self.print_log(json.dumps(data))
        elif command == 'add_status':
            pass
            #rprint("Adding status")
            """
            status_tab = self.tabs[self.status_tab]['instance']
            if self.status_idchart:
                status_widget = self.chart_manager[self.status_idchart]
                #gprint("Status widget")
                #bprint(status_widget)
                st_data = {msg['station']:data}
                status_widget.add_point(st_data)
                status_widget.refresh_traces()
                #status_widget.update()
            #self.status_idchart.refresh_traces()
            """
        elif command=='load_chart' and not self.chart_flag:
            #self.chart_flag=True
            #gprint("WS dd load chart")
            #self.init_chart()
            #self.init_status()
            time.sleep(1)
        elif command=='source_groups':
            msg=data['msg']
            process_id=data['process']
            group=data['group']
            if msg=='update':
                self.process_group.update({"process":process_id, "group":group})
        elif command=='add_data':
            stn=None
            # check dict structure
            if 'station_name' in data:
                self.print_data(data['station_name'])
                self.print_data(self.group)
                self.print_data(data)
                stn=data['station_name']
                if self.chart_flag and stn in self.group:
                    self.add_point(data)
                    self.renew_canvas()
            dtdt = now - self.new_datetime
            rprint("Verificando dt %s - %s, delta %s" % (now, self.new_datetime, str(dtdt.total_seconds()) ) )
            if dtdt.total_seconds()>0:
                # calcular mediana de los puntos por cada estacion
                ref_dic = self.compute_median()
                # enviar diccionario con datos de mediana a canal dbus
                str_rd = json.dumps(ref_dic)
                gprint("Enviando nuevo diccionario a mapa: %s" %str_rd)
                self.signal_ref.message(str_rd)
                # update next date
                self.new_datetime = datetime.now()+timedelta(minutes=1)
        else:
            print("No command on msg")
            print(msg)

        print("", flush=True)


    def add_point(self, msg):
        try:
            #gprint("WTT al añadir punto")
            #print("WTT %s"%self.charts[self.main_idchart]['instance'])
            if self.main_idchart in self.charts.keys():
                chart=self.charts[self.main_idchart]['instance']
                chart.add_point(msg)
        except Exception as e:
            gprint("WTT Error al añadir punto")
            raise e

    def compute_median(self):
        try:
            #gprint("WTT al añadir punto")
            #print("WTT %s"%self.charts[self.main_idchart]['instance'])
            if self.main_idchart in self.charts.keys():
                chart=self.charts[self.main_idchart]['instance']
                return chart.compute_median()
        except Exception as e:
            gprint("WTT Error al computar mediana")
            raise e


    def renew_canvas(self):
        #print("QQT Chart keys  %s" %self.charts.keys())
        if self.main_idchart in self.charts.keys():
            chart=self.charts[self.main_idchart]['instance']
            #print("QQT Chart  %s" %chart)
            chart.refresh_traces()
            #print("WTT Num figs post")
            chart.draw()

    def init_chart(self):
        #################DATA INPUT REF TO CHART
        # Is readed from database>
        # self.stations
        # self.position
        # self.guide
        ##############
        chart_args=[]
        #sl_stations=["VALN","QTAY","AEDA","ATJN"]
        #self.selected_sta=sl_stations
        #id_stations=[self.guide[s] for s in sl_stations]
        self.print_data(self.stations)

        #selection=['TRPD','ATJN','VALN','CRSC','RCSD','HSCO']

        selection=self.group
        
        keys_stations=[(key,s['code']) for (key, s) in self.stations.items() if s['code'] in selection]


        keys=[ k[0] for k in keys_stations ]
        stations=[ k[1] for k in keys_stations ]
        #self.selected_sta=stations
        print(stations)
        position=[(self.position[k]['llh']['lon'],self.position[k]['llh']['lat']) for k in keys]
        self.print_log("Estaciones a gráfico::::")
        self.print_log(json.dumps(stations))
        #define amount of stations
        n=self.nsta
        self.selected_sta=stations[:n]
        self.print_data(self.selected_sta)
        args=[self.selected_sta, position[:n]]
        chart_options={
            'widget':self.chart_type,
            'fnp':self.print_data,
            'display_window':600,
            'data_buffer':1200,
            'stations_per_canvas':n}
        gprint("RR Creating chart")
        idchart=self.create_chart(self.idt1, *args, **chart_options)
        self.main_idchart=idchart
        self.chart_flag=True

    def init_status(self):
        ids_codes=[(key, s['code']) for (key, s) in self.stations.items() if s['code'] in self.group]
        status_tab = self.tabs[self.status_tab]['instance']
        # args
        selection=self.group 
        keys_stations=[(key,s['code']) for (key, s) in self.stations.items() if s['code'] in selection]
        keys=[ k[0] for k in keys_stations ]
        stations=[ k[1] for k in keys_stations ]
        position=[(self.position[k]['llh']['lon'],self.position[k]['llh']['lat']) for k in keys]
        n=self.nsta
        self.selected_sta=stations[:n]
        self.print_data(self.selected_sta)
        args=[self.selected_sta, position[:n]]
        # chart_options
        chart_options = {
            'name': 'Status',
            'widget': 'gnss_status',
            'stationsXcolumn': 30,
            'lb': 25,
            'lm': 1
        }
        idchart=self.create_chart(self.status_tab, *args, **chart_options)
        self.status_idchart = idchart
        self.status_chart_flag = True

    @pyqtSlot()
    def on_click(self):
        print("PyQt5 button click")
        textboxValue=self.textbox.text()
        self.print_log(textboxValue)
        self.patrol.message({"msg":"Hola signal dbus %s" %textboxValue}) 

    @pyqtSlot()
    def cerrar(self):
        self.close()


    def send_init(self):
        q_send = self.queue_list[1]
        msg = {
            'command' : 'init_gui',
            'args':[self.idg, self.group]
        }
        print("Sending from GUI: %s" %msg)
        print("Queue : %s" %q_send)
        q_send.put(json.dumps(msg))
        msg={
            'command':'GET_STA',
            'args': []
        }
        rprint("INIT Request %s" %msg)
        q_send.put(json.dumps(msg))


 
def run_gui(data):
    #DBusQtMainLoop(set_as_default=True)
    app=QApplication(sys.argv)
    gui=Gui(**data)
    gui.showMaximized()
    sys.exit(app.exec_())


import time


class DataProcess():
    def __init__(self, *args, **kwargs):
        super().__init__()

    def run(self):
        self.signal=GatherSignal()
        count=0
        while True:
            self.signal.message({'msg':"Hello %s" %count})
            time.sleep(2)
            count+=1
            if count>=100:
                break

