from .manager import QTManager
from gnsocket.gn_socket import GNCSocket
from networktools.time import get_datetime_di
from datadbs.rethinkdb import Rethink_DBS
from networktools.library import my_random_string
from networktools.colorprint import gprint, bprint, rprint
import simplejson as json
import sys

from PyQt5.QtCore import QObject, pyqtSignal, QSharedMemory
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import pyqtSlot, Q_CLASSINFO
from PyQt5 import QtCore, QtDBus
from multiprocessing import Manager, Queue

from quamash import QEventLoop, QThreadExecutor
from tasktools.taskloop import coromask, renew, simple_fargs, renew_quamash

#from dbus.mainloop.pyqt5 import DBusQtMainLoop

import time
import copy
import queue
import asyncio
import functools

from datetime import datetime as dt


# Async Socket
from rethinkdb import RethinkDB
rdb = RethinkDB()
rdb.set_loop_type('asyncio')

# Async Socket


#from .manager import QTManager


# D-bus chapter
service = 'csn.atlas.gather'
interface = '%s.interface' % service


class GatherDBusAdaptor(QtDBus.QDBusAbstractAdaptor):
    Q_CLASSINFO("D-Bus Interface", interface)
    Q_CLASSINFO("D-Bus Introspection", ''
                '  <interface name="csn.atlas.gather.interface">\n'
                '	 <signal name="message">\n'
                '	   <arg type="s" name="data"/>\n'
                '	 </signal>\n'
                '  </interface>\n'
                '')
    message = pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.setAutoRelaySignals(True)
        print("Abstract adaptor")


class GatherDBusAdaptorRef(QtDBus.QDBusAbstractAdaptor):
    Q_CLASSINFO("D-Bus Interface", "csn.gui.coord.interface")
    Q_CLASSINFO("D-Bus Introspection", ''
                '  <interface name="csn.gui.coord.interface">\n'
                '	 <signal name="message">\n'
                '	   <arg type="s" name="data"/>\n'
                '	 </signal>\n'
                '  </interface>\n'
                '')
    message = pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)
        self.setAutoRelaySignals(True)
        print("Abstract adaptor REF")


class GatherDBusAdaptorRef(QtDBus.QDBusAbstractAdaptor):
    Q_CLASSINFO("D-Bus Interface", 'csn.gui.coord.interface')
    Q_CLASSINFO("D-Bus Introspection", ''
                '  <interface name="csn.gui.coord.interface">\n'
                '	 <signal name="message">\n'
                '	   <arg type="a{sv}" name="data"/>\n'
                '	 </signal>\n'
                '  </interface>\n'
                '')
    message = pyqtSignal(list)

    def __init__(self, parent):
        super().__init__(parent)
        self.setAutoRelaySignals(True)
        print("Abstract adaptor")


class GatherDBusInterfaceRef(QtDBus.QDBusAbstractInterface):
    message = pyqtSignal(list)

    def __init__(self, service, path, interface, connection, parent=None):
        super().__init__(service, path, interface, connection, parent)
        print("Gather DBus Adaptor ok")


class GatherDBusInterface(QtDBus.QDBusAbstractInterface):
    message = pyqtSignal(str)

    def __init__(self, service, path, interface, connection, parent=None):
        super().__init__(service, path, interface, connection, parent)
        print("Gather DBus Adaptor ok")


class GatherSignal(QObject):
    signal = pyqtSignal(dict)
    message = pyqtSignal(str)

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent)
        self.path = "/data"
        if 'path' in kwargs.keys():
            self.path = kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service = kwargs['service']
        self.interface = "%s.interface" % self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
            self.variable = kwargs['variable']
        GatherDBusAdaptor(self)
        self.connection = QtDBus.QDBusConnection.sessionBus()
        self.connection.registerObject(self.path, self)
        self.connection.registerService(self.service)
        self.iface = GatherDBusInterface(
            self.service, self.path, self.interface, self.connection, self)
        self.connect(self.send)

    def message(self, msg):
        self.message.emit(msg)

    @pyqtSlot(dict)
    def send(self, msg):
        data = msg  # json.dumps(msg)
        print("send to dbus %s" % data)
        self.emit([data])

    def emit(self, data):
        msg = QtDBus.QDBusMessage.createSignal(
            self.path, self.interface, self.variable)
        msg << data
        print(msg)
        self.connection.send(msg)

    def connect(self, method):
        self.signal.connect(method)


class GuiMap(QMainWindow, QTManager):
    signal = pyqtSignal(str)
    message = pyqtSignal(dict)

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        # dbus variables
        self.path = "/data"
        if 'path' in kwargs.keys():
            self.path = kwargs['path']
        self.service = service
        if 'service' in kwargs.keys():
            self.service = kwargs['service']
        self.interface = "%s.interface" % self.service
        self.variable = "message"
        if 'variable' in kwargs.keys():
            self.variable = kwargs['variable']
        # dbus ref variables
        self.path_ref = "/data"
        if 'path_ref' in kwargs.keys():
            self.path_ref = kwargs['path_ref']
        self.service_ref = 'csn.gui.coord'
        if 'service_ref' in kwargs.keys():
            self.service_ref = kwargs['service_ref']
        self.interface_ref = "%s.interface" % self.service_ref
        self.variable_ref = "message"
        if 'variable_ref' in kwargs.keys():
            self.variable_ref = kwargs['variable_ref']
        # other variables
        if 'size' in kwargs.keys():
            self.size = kwargs['size']
        if 'nen' in kwargs.keys():
            self.nen = kwargs['nen']
        self.chart_type = 'gnss_map'
        if 'chart' in kwargs.keys():
            self.chart_type = kwargs['chart']
        if 'group' in kwargs.keys():
            self.group = kwargs['group']
        if 'queue_set' in kwargs.keys():
            self.queue_set = kwargs['queue_set']
        if 'rdb_address' in kwargs.keys():
            self.rethinkdb_address = kwargs['rdb_address']
        if 'rdb_dbname' in kwargs.keys():
            self.rethinkdb_dbname = kwargs['rdb_dbname']
        if 'queue_list' in kwargs.keys():
            self.queue_list = kwargs['queue_list']
        self.idg = 'gui'
        if 'id_gui' in kwargs.keys():
            self.idg = kwargs["id_gui"]
        if 'type_bus' in kwargs.keys():
            self.type_bus = kwargs['type_bus']
        # get set ov variables to control map gui
        self.read_queue_map = kwargs.get('rqm', Queue())
        self.write_queue_map = kwargs.get('wqm', Queue())
        self.map_status = kwargs.get('map_status', 'OFF')
        self.dbus_opt_ref = kwargs.get('dbus_ref', {})
        # OK
        self.nsta = len(self.group)
        self.data_queue = queue.Queue(maxsize=6)
        self.title = 'DragonCharts GNSS Real Time System Map'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.stations = {}
        self.assign_stations = {}
        self.process_group = {}
        self.position = {}
        self.guide = {}
        self.selected_sta = []
        self.chart_flag = False
        self.status_idchart = None
        self.di = rdb.iso8601(get_datetime_di(delta=30))
        self.last_time = {sta: self.di for sta in self.group}
        self.patrol = GatherSignal(self)
        self.main_idchart = None
        # self.up_gui()
        self.st_up = True
        self.clear_dbus_ref()
        self.initUI()

    def setSignal(self, signal):
        #print("Signal %s" %signal)
        self.message = signal

    def clear_dbus_ref(self):
        if self.dbus_opt_ref:
            self.service_ref = self.dbus_opt_ref['service']
            self.interface_ref = self.dbus_opt_ref['interface']
            self.variable_ref = self.dbus_opt_ref['variable']
            self.path_ref = self.dbus_opt_ref['path']
            bprint("Parámetros dbus ref")
            rprint(["service", "path", "interface", "variable"])
            bprint([self.service_ref, self.path_ref,
                    self.interface_ref, self.variable_ref])

    def initUI(self):
        self.main_widget = QWidget()
        # create button
        # create button
        button_close = QPushButton("Cerrar")
        button_close.setToolTip("Cerrar Interfaz")
        button_close.clicked.connect(self.cerrar)
        # create textbox
        self.textbox = QLineEdit()
        self.textbox.resize(280, 40)
        wn_options = {'direction': 2}
        args = [QBoxLayout.TopToBottom]
        idl = self.create_layout(None, *args,  **wn_options)
        vbox = self.layouts[idl]['instance']
        vbox.addWidget(button_close)
        vbox.addWidget(self.textbox)
        ts_args = []
        ts_options = {}
        idts = self.create_tabset(idl, *ts_args, **ts_options)
        # Tab for first chart:>
        t1_args = [QBoxLayout.LeftToRight]
        t1_options = {'name': 'Gráficos 1'}
        idt1 = self.create_tab(idts, *t1_args, **t1_options)
        self.idt1 = idt1
        # Tab for log and stations log:>
        t2_args = [QBoxLayout.LeftToRight]
        t2_options = {'name': 'Log'}
        idt2 = self.create_tab(idts, *t2_args, **t2_options)
        # Tab for data log:>
        t3_args = [QBoxLayout.LeftToRight]
        t3_options = {'name': 'Data Log'}
        idt3 = self.create_tab(idts, *t3_args, **t3_options)
        # Text for Log:>
        text_args = []
        text_options = {}
        idtext = self.create_text(idt2, *text_args, **text_options)
        # Text for data log:>
        text_args2 = []
        text_options2 = {}
        idtext2 = self.create_text(idt3, *text_args2, **text_options2)
        self.log_text = idtext
        self.log_data = idtext2
        self.text = self.log_data
        #  Set main window
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage("Mensaje en Status Bar")
        # show
        self.connect_dbus_ref2()
        self.connect_dbus()
        # self.connect_dbus_ref()
        self.send_init()
        self.show()

    def connect_dbus_ref2(self):
        # call adaptor for this channel
        GatherDBusAdaptorRef(self)
        # create connection
        self.connection2 = QtDBus.QDBusConnection.sessionBus()
        path = "/lala"
        service = 'csn.coord'
        interface = "%s.interface" % service
        variable = 'message'

        objectReg = self.connection2.objectRegisteredAt(path)
        self.connection2.unregisterObject(path)
        "=============="
        regObj = self.connection2.registerObject(
            path, interface, self)  # register path, object
        regSess = self.connection2.registerService(service)  # register service
        # call DBUS interface with my service, path, interface assigned to this object
        self.server2 = GatherDBusInterfaceRef(
            service,
            path,
            interface,
            self.connection2,
            self)
        # connect dbus connection to slot
        self.connection2.connect(
            service,
            path,
            interface,
            variable,
            self.update_data_2)  # (service, path, interface, variable, slot_fn)
        rprint(["service", "path", "interface", "variable"])
        bprint([service, path, interface, variable])
        if not QtDBus.QDBusConnection.sessionBus().isConnected():  # check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" % service)

    @pyqtSlot(list)
    def update_data_2(self, msg):
        pass

    def connect_dbus(self):
        bprint("Dbus init")
        # call adaptor for this channel
        GatherDBusAdaptor(self)
        # create connection
        self.connection = QtDBus.QDBusConnection.sessionBus()
        objectReg = self.connection.objectRegisteredAt(self.path)
        # self.connection.unregisterObject(self.path)
        "=============="
        regObj = self.connection.registerObject(
            self.path, self.interface, self)  # register path, object
        regSess = self.connection.registerService(
            self.service)  # register service
        # call DBUS interface with my service, path, interface assigned to this object
        self.server = GatherDBusInterface(
            self.service,
            self.path,
            self.interface,
            self.connection,
            self)
        # connect dbus connection to slot
        rprint("Interface ok")
        if not QtDBus.QDBusConnection.sessionBus().isConnected():  # check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" % self.service)

        self.connection.connect(
            self.service,
            self.path,
            self.interface,
            self.variable,
            self.update_data)  # (service, path, interface, variable, slot_fn)
        rprint(["service", "path", "interface", "variable"])
        bprint([self.service, self.path, self.interface, self.variable])
        if not QtDBus.QDBusConnection.sessionBus().isConnected():  # check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" % self.service)
        gprint("Interface connected")
        print(self.server)
        print(self.server.message)
        # connect interface message signal to slot
        # self.server.message.connect(self.update_data)
        print("Server message %s" % self.server.message)
        # send a message
        #self.message.emit({"hola":"test message"})

    def connect_dbus_ref(self):
        # call adaptor for this channel
        GatherDBusAdaptorRef(self)
        # create connection
        self.connection_ref = QtDBus.QDBusConnection.sessionBus()
        objectReg = self.connection_ref.objectRegisteredAt(self.path_ref)
        self.connection_ref.unregisterObject(self.path_ref)
        "=============="
        regObj = self.connection_ref.registerObject(
            self.path_ref, self.interface_ref, self)  # register path, object
        regSess = self.connection_ref.registerService(
            self.service_ref)  # register service
        # call DBUS interface with my service, path, interface assigned to this object
        self.server_ref = GatherDBusInterfaceRef(
            self.service_ref,
            self.path_ref,
            self.interface_ref,
            self.connection_ref,
            self)
        # connect dbus connection to slot
        self.connection_ref.connect(
            self.service_ref,
            self.path_ref,
            self.interface_ref,
            self.variable_ref,
            self.update_data_ref)  # (service, path, interface, variable, slot_fn)
        rprint(["service", "path", "interface", "variable"])
        bprint([self.service_ref, self.path_ref,
                self.interface_ref, self.variable_ref])
        if not QtDBus.QDBusConnection.sessionBus().isConnected():  # check if connected
            print("Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" % self.service_ref)

    @pyqtSlot()
    def printhola(self):
        self.signal.emit("Hola\n")
        self.patrol.message({"msg": "desde gui"})
        bprint(self.main_chart)

    @pyqtSlot(list)
    def update_data_ref(self, data):
        gprint("Receiving new data ref dict %s" % data)
        gprint("Receiving new data ref dict %s" % type(data))
        rprint(["service_ref", "path_ref", "interface_ref", "variable_ref"])
        bprint([self.service_ref, self.path_ref,
                self.interface_ref, self.variable_ref])
        for elem in data:
            msg = elem.get('msg', None)
            command = elem.get('command', None)
            ref_dict = {}
            rprint("MSG %s " % msg)
            rprint("CMD %s " % command)
            if msg and not command == 'msg':
                bprint("Mensaje input %s -> type %s" % (msg, type(msg)))
                ref_dict = json.loads(msg)
            if command == 'msg':
                gprint("Nuevo mensaje ref")
                bprint(msg)
            elif command == 'new_ref':
                rprint("Adding new ref")
                new_data = ref_dict
                # get map widget
                chart = None
                try:
                    #gprint("WTT al añadir punto")
                    #print("WTT %s"%self.charts[self.main_idchart]['instance'])
                    if self.main_idchart in self.charts.keys():
                        gprint("Integrando nueva referencia  %s" % new_data)
                        chart = self.charts[self.main_idchart]['instance']
                        chart.update_ref(new_data)
                except Exception as e:
                    gprint("WTT Error al añadir punto")
                    raise e
                # run method to update to new_data

    @pyqtSlot(str)
    def update_data(self, data):
        #bprint("Data updating %s" %data)
        new_data = json.loads(data)
        msg = new_data
        command = msg['command']
        data = msg['data']
        # self.print_data(new_data)
        if command == 'station':
            self.stations.update(data)
            station = tuple(data.items())
            self.guide.update({
                station[0][1]['code']: station[0][0]})
            station_ids = list(msg.keys())[0]
            self.print_log(json.dumps(data))
            if self.st_up:
                bprint("Activando dbus reference channel")
                self.connect_dbus_ref()
                self.st_up = False
        elif command == 'position':
            self.position.update(data)
            # self.print_log(json.dumps(data))
        elif command == 'load_chart' and not self.chart_flag:
            self.chart_flag = True
            #gprint("WS dd load chart")
            self.init_chart()
            # self.init_status()
            time.sleep(1)
        elif command == 'source_groups':
            msg = data['msg']
            process_id = data['process']
            group = data['group']
            if msg == 'update':
                self.process_group.update(
                    {"process": process_id, "group": group})
        elif command == 'add_data':
            stn = None
            # check dict structure
            if 'station_name' in data:
                # self.print_data(data['station_name'])
                # self.print_data(self.group)
                # self.print_data(data)
                stn = data['station_name']
                if self.chart_flag and stn in self.group:
                    self.add_point(data)
                    # self.renew_canvas()
        else:
            print("No command on msg")
            print(msg)

    def add_point(self, msg):
        try:
            #gprint("WTT al añadir punto")
            #print("WTT %s"%self.charts[self.main_idchart]['instance'])
            if self.main_idchart in self.charts.keys():
                chart = self.charts[self.main_idchart]['instance']
                chart.add_point(msg)
        except Exception as e:
            gprint("WTT Error al añadir punto")
            raise e

    def renew_canvas(self):
        #print("QQT Chart keys  %s" %self.charts.keys())
        if self.main_idchart in self.charts.keys():
            chart = self.charts[self.main_idchart]['instance']
            #print("QQT Chart  %s" %chart)
            chart.refresh_traces()
            #print("WTT Num figs post")
            chart.draw()

    def init_chart(self):
        # DATA INPUT REF TO CHART
        # Is readed from database>
        # self.stations
        # self.position
        # self.guide
        ##############
        chart_args = []
        # sl_stations=["VALN","QTAY","AEDA","ATJN"]
        # self.selected_sta=sl_stations
        #id_stations=[self.guide[s] for s in sl_stations]
        self.print_data(self.stations)

        # selection=['TRPD','ATJN','VALN','CRSC','RCSD','HSCO']

        selection = self.group

        keys_stations = [(key, s['code']) for (key, s)
                         in self.stations.items() if s['code'] in selection]

        keys = [k[0] for k in keys_stations]
        stations = [k[1] for k in keys_stations]
        # self.selected_sta=stations
        print(stations)
        position = [(self.position[k]['llh']['lon'],
                     self.position[k]['llh']['lat']) for k in keys]
        self.print_log("Estaciones a gráfico::::")
        self.print_log(json.dumps(stations))
        # define amount of stations
        n = self.nsta
        self.selected_sta = stations[:n]
        self.print_data(self.selected_sta)
        args = [self.selected_sta, position[:n]]
        chart_options = {
            'widget': self.chart_type,
            'fnp': self.print_data,
            'display_window': 600,
            'data_buffer': 1200,
            'stations_per_canvas': n}
        gprint("RR Creating chart")
        idchart = self.create_chart(self.idt1, *args, **chart_options)
        self.main_idchart = idchart
        self.chart_flag = True

    @pyqtSlot()
    def on_click(self):
        print("PyQt5 button click")
        textboxValue = self.textbox.text()
        self.print_log(textboxValue)
        self.patrol.message({"msg": "Hola signal dbus %s" % textboxValue})
        bprint("Widget chart %s" % self.main_idchart)
        widget = self.chart_manager[self.main_idchart]
        rprint("Widget chart manager %s" %
               self.chart_manager[self.main_idchart])
        gprint("Widget chart geometry %s" %
               self.chart_manager[self.main_idchart].frameGeometry())
        bprint("Widget chart geometry x %s" %
               self.chart_manager[self.main_idchart].frameGeometry().width())
        bprint("Widget chart geometry y %s" %
               self.chart_manager[self.main_idchart].frameGeometry().height())
        widget.resize(800, 1200)

    @pyqtSlot()
    def cerrar(self):
        self.close()

    def send_init(self):
        q_send = self.queue_list[1]
        msg = {
            'command': 'init_gui',
            'args': [self.idg, self.group]
        }
        print("Sending from GUI: %s" % msg)
        print("Queue : %s" % q_send)
        q_send.put(json.dumps(msg))
        msg = {
            'command': 'GET_STA',
            'args': []
        }
        rprint("INIT Request %s" % msg)
        q_send.put(json.dumps(msg))
