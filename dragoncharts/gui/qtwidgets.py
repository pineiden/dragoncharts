from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QPlainTextDocumentLayout
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem
from PyQt5.QtWidgets import QSizePolicy


from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

import random
import matplotlib as plt
plt.use('Qt5Agg')

from PyQt5 import QtCore, QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import numpy as np
from gnss_traces.gnss_plots import GNSS_Display

from gnss_plots.traces import GnssPlots
from gnss_plots.status_chart import StatusChart
#import map
from gnss_map.gnss_map import GnssMap

from networktools.colorprint import gprint, bprint, rprint

class MyMplCanvas(FigureCanvas):
    def __init__(self, figure=None, parent=None, width=5, height=4, dpi=100,*args,**kwargs):
        if figure:
            fig=figure
        else:
            fig = Figure(figsize=(width, height), dpi=dpi)
        #self.axes = fig.add_subplot(111)
        # We want the axes cleared every time plot() is called
        #self.axes.hold(False)

        self.compute_initial_figure()

        #
        super(MyMplCanvas,self).__init__(fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)

        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass

class MyGraphCanvas():
    pass


class ChartWidget(MyMplCanvas):
    def __init__(self,*args,**kwargs):
        pass

class SinChartWidget(MyMplCanvas):
    def compute_initial_figure(self):
        t = np.arange(0.0,3.0,0.01)
        s= np.sin(2*np.pi*t)
        self.axes.plot(t,s)

class DynamicChartWidget(MyMplCanvas):
        """A canvas that updates itself every second with a new plot."""
        def __init__(self, *args, **kwargs):
            MyMplCanvas.__init__(self, *args, **kwargs)
            timer = QtCore.QTimer(self)
            timer.timeout.connect(self.update_figure)
            timer.start(1000)

        def compute_initial_figure(self):
            self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')

        def update_figure(self):
            # Build a list of 4 random integers between 0 and 10 (both inclusive)
            l = [random.randint(0, 10) for i in range(4)]

            self.axes.plot([0, 1, 2, 3], l, 'r')
            self.draw()

class GNSSChartWidget:
    def __init__(self, *args, **kwargs):
        self.gnss=GNSS_Display(
            display_window=kwargs['display_window'],
            data_buffer=kwargs['data_buffer'],
            stations_per_canvas=kwargs['stations_per_canvas'])
        print(self.gnss)
        self.least_stations=False
        self.widget_set=[]

    def add_station(self, st_code, coords):
        self.gnss.add_station(st_code, coords)
        self.least_stations=True

    def create_canvas(self):
        widget_set=[]
        for gnss_fig in self.gnss.gnss_figs:
            new_widget=MyMplCanvas(figure=gnss_fig.fig)
            widget_set.append(new_widget)
        self.widget_set=widget_set

    def refresh_traces(self):
        self.gnss.refresh_all()

    def draw(self):
        #print("WTT widget set %s" % self.widget_set)
        try:
            for w in self.widget_set:
                w.draw()
        except Exception as e:
            print("WTT Excepcion en draw chart")
            raise e

    def compute_initial_figure(self):
        pass

    def add_point(self, msg):
        #print(msg)
        try:
            if msg:
                self.gnss.add_point(msg)
        except Exception as exec:
            print("Problemas en add point")
            print(exec)
            raise exec

class GuiQWidget(QWidget):
    def __init__(self,*args,**kwargs):
        #filter only avalaibles options to QWidget
        gprint("Kwargs qwidget %s" %kwargs)
        super(GuiQWidget, self).__init__(**kwargs)


class GNSSGraphWidget:
    def __init__(self, *args, **kwargs):
        self.gnss=GnssPlots(
            trace_length=kwargs['display_window'],
            buffer_length=kwargs['data_buffer'],
            stations_per_canvas=kwargs['stations_per_canvas'])
        print(self.gnss)
        self.least_stations=False
        self.widget_set=[]

    def add_station(self, st_code, coords):
        self.gnss.add_station(st_code, [coords[1],coords[0]])
        self.least_stations=True

    def create_canvas(self):
        widget_set=[]
        for gnss_fig in self.gnss.figs:
            new_widget=gnss_fig
            widget_set.append(new_widget)
        self.widget_set=widget_set

    def refresh_traces(self):
        self.gnss.refresh_all()

    def draw(self):
        #print("WTT widget set %s" % self.widget_set)
        try:
            for w in self.widget_set:
                pass
                #w.draw()
        except Exception as e:
            print("WTT Excepcion en draw chart")
            raise e

    def compute_initial_figure(self):
        pass

    def add_point(self, msg):
        #bprint(msg)
        try:
            if msg:
                self.gnss.set_data(msg)
        except Exception as exec:
            print("Problemas en add point")
            print(exec)
            raise exec

    def compute_median(self):
        return self.gnss.compute_reference_enu()


class GNSSStatusWidget(QWidget):
    """
    Status Matplotlib widget 
    """
    def __init__(self, *args, **kwargs):
        bprint("Status kwargs")
        gprint(kwargs)
        super(GNSSStatusWidget, self).__init__(**{})
        self.spc = kwargs.get('stationsXcolumn', 30)
        self.cols = kwargs.get('cols', 3)
        self.lb = kwargs.get('lb', 25)
        # lm es el valor de alerta para mostrar barra en rojo
        # _mem_scale_floor = 3.  # memory scale's floor in months
        # lm > mem_scale_floor
        self.lm = kwargs.get('lm', 1)
        self.status = StatusChart(
            stations_per_col = self.spc,
            cols = self.cols,
            low_batt = self.lb,
            low_mem_months = self.lm,
            n_refresh = 5
        )

        self.least_stations=False
        self.widget_set=[]

    def add_station(self, st_code, coords):
        self.status.add_station(st_code, coords)
        self.least_stations=True

    def create_canvas(self):
        widget_set=[]
        for gnss_fig in [self.status.fig]:
            new_widget=MyMplCanvas(figure=gnss_fig)
            widget_set.append(new_widget)
        self.widget_set=widget_set

    def refresh_traces(self):
        self.status.refresh()

    def draw(self):
        print("WTT widget set %s" % self.widget_set)
        try:
            for w in self.widget_set:
                pass
                #w.draw()
        except Exception as e:
            print("WTT Excepcion en draw chart")
            raise e

    def compute_initial_figure(self):
        pass

    def add_point(self, msg):
        print(msg)
        try:
            if msg:
                rprint("Set new data %s" %msg)
                self.status.set_data(msg)
        except Exception as exec:
            print("Problemas en add point")
            print(exec)
            raise exec


class GnssMapWidget(QWidget):
    def __init__(self, *args, **kwargs):
        bprint("Status kwargs")
        gprint(kwargs)
        super(GnssMapWidget, self).__init__(**{})
        self.detail = kwargs.get('detail', 5)
        self.region = kwargs.get('region', 'CL')
        self.gnss_map = GnssMap(
            detail = self.detail,
            region = self.region,
        )
        self.least_stations = False
        self.widget_set = []

    def add_station(self, st_code, coords):
        self.gnss_map.add_station(st_code,
                                  (coords[1], coords[0]),
                                  refresh_arrows=True)
        self.least_stations = True

    def create_canvas(self):
        widget_set = []
        for gnss_fig in [self.gnss_map.fig]:
            new_widget = MyMplCanvas(figure=gnss_fig)
            widget_set.append(new_widget)
        self.widget_set = widget_set

    def refresh_traces(self):
        self.gnss_map.refresh_arrows()
        self.gnss_map.connect_event_actions()

    def draw(self):
        #print("WTT widget set %s" % self.widget_set)
        try:
            for w in self.widget_set:
                pass
                #w.draw()
        except Exception as e:
            print("WTT Excepcion en draw chart")
            raise e

    def compute_initial_figure(self):
        pass

    def add_point(self, msg):
        #print(msg)
        try:
            if msg:
                #rprint("Set new data %s" %msg)
                self.gnss_map.set_data(msg)
        except Exception as exec:
            print("Problemas en add point")
            print(exec)
            raise exec

    def resizeEvent(self, event):
        rprint("Evento resize: %s" %event)
        super(GnssMapWidget, self).resizeEvent(event)
        self.gnss_map.action_resize(event.width, event.height)


    def update_ref(self, new_dict):
        self.gnss_map.update_ref_enu(new_dict)
