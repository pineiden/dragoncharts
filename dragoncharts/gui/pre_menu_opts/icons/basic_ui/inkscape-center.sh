#!/bin/bash
# inkscape-center <file-or-directory>...

_analyse() {
    if [ -d "${1}" ] ; then
        _centerAll "${1}" ;
    else
        _center "${1}" ;
    fi
}

_centerAll() {
    cd "${1}" ;
    for img in $(ls "*.svg") ; do
        _filterSyms "${img}" ;
    done
}

_filterSyms() {
    if [[ $(readlink "${1}") == "" ]] ; then
        _center "${1}"
    fi
}

_center() {
    inkscape --verb=EditSelectAll --verb=AlignHorizontalCenter --verb=AlignVerticalCenter --verb=FileSave --verb=FileQuit "${1}"
}

for arg ; do
    _analyse "${arg}" ;
done
