from PySide2.QtWidgets import (QTableWidgetItem, QStyle,
                               QStyleOptionButton, QCheckBox, QStyledItemDelegate)
from PySide2.QtCore import Qt as qt


class CheckBoxDelegate(QStyledItemDelegate):
    role = qt.CheckStateRole

    def __init__(self, option_widget, parent=None, *args, **kwargs):
        super().__init__(option_widget, parent)

    def flags(self, index):
        return qt.ItemIsEditable | qt.ItemIsUserCheckable | qt.ItemIsEnabled

    def createEditor(self, parent, option, index):
        '''
        Important, otherwise an editor is created if the user clicks in this cell.
        ** Need to hook up a signal to the model
        '''
        return None

    def paint(self, painter, option, index):
        """
        Load data and draw over item
        Paint a checkbox without the label
        """

        item = QTableWidgetItem()
        item.setFlags(qt.ItemIsUserCheckable | qt.ItemIsEnabled)

        # picks up data from database
        checked = index.model().data(index, role=qt.DisplayRole) == 'True'

        # Then draw the checkbox
        box = QStyleOptionButton()

        box.palette = option.palette
        box.rect = option.rect
        box.state = QtGui.QStyle.State_Enabled

        if checked:
            box.state |= QStyle.State_On
        else:
            box.state |= QStyle.State_Off

        style = QtGui.QApplication.instance().style()
        style.drawControl(qt.CE_CheckBox, box, painter)
        painter.restore()
