from networktools.text import babosear
import sys
import asyncio
import queue
from PySide2.QtCore import QObject, QAbstractTableModel, Qt as qt
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import (QSize,  QTimer, SIGNAL, SLOT)
from PySide2.QtGui import QIcon, QWindow
from PySide2.QtWidgets import QDialog, QDialogButtonBox, QErrorMessage
from PySide2.QtWidgets import QTableView, QTableWidgetItem
from PySide2.QtWidgets import (QPlainTextEdit,
                               QPlainTextDocumentLayout,
                               QApplication,
                               QDialog,
                               QWidget,
                               QMainWindow,
                               QPushButton,
                               QMessageBox,
                               QAction,
                               QLineEdit,
                               QGroupBox,
                               QTabWidget,
                               QTableWidget,
                               QTableWidgetItem,
                               QBoxLayout,
                               QVBoxLayout,
                               QGridLayout,
                               QMenuBar,
                               QMenu,
                               QSpinBox,
                               QLabel,
                               QHeaderView)

from layout_wrapper import (BoxLayout,
                            HBoxLayout,
                            VBoxLayout,
                            GridLayout,
                            FormLayout)

from PySide2.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor

from container_table import DataModel, ManageData


AEventLoop = type(asyncio.get_event_loop())

# Una clase intermedia o bridge para juntar
# los eventloop async


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


# Declarar la creación de la interfaz
# QMainWindow permite crear una clase
# que levanta una ventana
# QWdidget > QMainWindow
#
# http://pyqt.sourceforge.net/Docs/PyQt4/qmainwindow.html#details
"""
Provee un marco de trabajo para construir la app, además existen las clases relacionadas
Además tiene su propio Layout, en el cual se peude agregar QToolBars, QDockWidgets, QMenuBar, QStatusBar
Layout contiene una zona central que puede ser ocupada por cualquier widget

http://pyqt.sourceforge.net/Docs/PyQt4/qmainwindow.html

"""

header = ['code', 'station_name', 'lat', 'lon']
values = [
    ('ADBV', 'Adubai', 124, 541),
    ('QURD', 'Queretaro', -45, 67),
    ('MANI', 'Managua', 45, 81),
    ('URYA', 'Haciendo Uruguay', 10, -45)
]
list_values = [dict(zip(header, value)) for value in values]


class Container(QMainWindow):
    def __init__(self, manager_data, model_name,
                 parent=None, *args, **kwargs):
        super().__init__(parent)
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.setGeometry(0, 0, self.width, self.height)
        self.name = kwargs.get('title')
        self.slug = kwargs.get('code')
        self.setWindowTitle(self.name)
        self.data_manager = manager_data
        self.model_name = model_name
        self.create_table()

    def create_table(self):
        idt = self.data_manager.init_table(
            'Table Principal', self, model_name=self.model_name)
        model = self.data_manager.get_model(idt)
        self.main_table = idt
        self.event_table(idt)

    def event_table(self, idt):
        table_widget = self.data_manager.get_table(idt)
        self.setCentralWidget(table_widget)
        table_widget.doubleClicked.connect(self.check_celda)

    def check_celda(self, item):
        print("Celda checkeada selected")
        print(item.data())
        model = self.data_manager.get_model(self.main_table)
        model.change_checkbox(item)

    def add_data(self, idd, data):
        idt = self.main_table
        model = self.data_manager.get_model(idt)
        model.add_data_row(idd, data)

    def drop_data(self, idd):
        print("Drop data on idd:>", idd)
        idt = self.main_table
        model = self.data_manager.get_model(idt)
        print(model.data_set)
        model.delete_data(idd)


class WindowForm(QMainWindow):
    form_opts = {'title': "Gráfica GNSS",
                 'code': 'grafica_gnss',
                 'command': 'create_container',
                 'position': None,
                 'checkbox': True,
                 'height': 400,
                 'width': 600}

    def __init__(self, queue, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.queue = queue
        self.setGeometry(0, 0, 400, 100)
        self.setWindowTitle("Crear Contenedor de Gráficas")
        self.create_form()
        self.end_event = "No se crea"

    def create_form(self):
        central_widget = QWidget(self)
        central_widget.setLayout(QGridLayout())
        self.setCentralWidget(central_widget)
        central_layout = central_widget.layout()
        central_layout.setRowStretch(1, 1)
        title_label = QLabel('Nombre de Ventana')
        window_title = QLineEdit(self)
        window_title.setText(self.form_opts.get('title'))
        self.title = window_title
        window_title.textChanged.connect(self.text_change)
        central_layout.addWidget(title_label, 1, 0)
        central_layout.addWidget(window_title, 1, 1)
        size_label = QLabel('Tamaño')
        central_layout.addWidget(size_label, 2, 0, 1, 1)
        w_min = 500
        w_max = 1600
        w_label = QLabel('Ancho [%d,%d]:' % (w_min, w_max))
        width = QSpinBox(self)
        width.setMinimum(w_min)
        width.setMaximum(w_max)
        width.valueChanged.connect(self.w_value_change)
        h_min = 400
        h_max = 1200
        h_label = QLabel('Alto: [%d, %d]' % (h_min, h_max))
        height = QSpinBox(self)
        height.setMinimum(h_min)
        height.setMaximum(h_max)
        height.valueChanged.connect(self.h_value_change)
        central_layout.addWidget(w_label, 3, 0)
        central_layout.addWidget(h_label, 3, 1)
        central_layout.addWidget(width, 4, 0)
        central_layout.addWidget(height, 4, 1)
        last_row = 4
        self.add_buttons(central_layout, last_row+1)

    def add_buttons(self, layout, row):
        # crear botón cerrar
        button_cerrar = QPushButton('Cerrar', self)
        layout.addWidget(button_cerrar, row, 0)
        button_cerrar.clicked.connect(self.close)
        # crear botón Crear Ventanta
        button_nw = QPushButton("Crear Ventanta", self)
        layout.addWidget(button_nw, row, 1)
        button_nw.clicked.connect(self.create_nw)

    def create_nw(self):
        self.form_opts.update({'command': 'create_container'})
        title = self.form_opts.get('title')
        self.form_opts.update({'code': babosear(title)})
        self.queue.put(self.form_opts)
        self.end_event = "Se crea Ventana"
        self.close()

    def get_opts(self):
        return self.form_opts

    def text_change(self, value):
        self.form_opts.update({'title': value})

    def w_value_change(self, value):
        self.form_opts.update({'width': value})

    def h_value_change(self, value):
        self.form_opts.update({'height': value})

    def closeEvent(self, event):
        print("Cerrando For Nueva Ventana > %s" % self.end_event)


class MainDataModel(DataModel):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        new_header = ["code", "station_name"]
        self.set_header(new_header, action='replace')
        self.set_static_header({0, 1})


class Gui(QMainWindow):
    main_table = None
    containers = dict()

    def __init__(self, list_values, parent=None, *args, **kwargs):
        super().__init__(parent)
        # up -> MenuBar
        # around -> Toolbars
        # bottom -> Status Bar
        self.queue = kwargs.get('queue')
        self._status_update_timer = None
        ManageData.set_default_model(MainDataModel)
        self.data_manager = ManageData(parent=self)
        self.create_menu()
        self.valid_set = self.load_data(list_values)

    def create_menu(self):
        menu_bar = self.menuBar()
        # menu != acción
        # Agregar menu
        icon_path = './icons/basic_ui/SVGs/{name}.svg'
        menu_opciones = menu_bar.addMenu('Opciones')
        # send -> self.opt_signal.emit(X)
        # Some standar actions on menu:
        # menu_opciones.hovered.connect(self.hover_opciones)
        menu_opciones.triggered.connect(self.trigger_opciones)
        # icon new_window
        nw_icon = QIcon(icon_path.format(name='Star'))
        nueva_ventana = menu_opciones.addAction(nw_icon, 'Nueva Ventana')
        nueva_ventana.triggered.connect(self.nueva_ventana)
        save_icon = QIcon(icon_path.format(name='Cog'))
        save_settings = menu_opciones.addAction(
            save_icon, 'Guardar Configuración')
        load_icon = QIcon(icon_path.format(name='File'))
        load_settings = menu_opciones.addAction(
            load_icon, 'Cargar Configuración')
        idt = self.data_manager.init_table('Table Principal', self)
        self.main_table = idt
        self.event_table(idt)

    def event_table(self, idt):
        table_widget = self.data_manager.get_table(idt)
        self.setCentralWidget(table_widget)
        table_widget.doubleClicked.connect(self.check_celda)

    def check_celda(self, item):
        model = self.data_manager.get_model(self.main_table)
        value, idd, data, column_name = model.change_checkbox(item)
        container = self.containers.get(column_name)
        if value:
            container.add_data(idd, data)
        else:
            container.drop_data(idd)
        # get column_name and boolean value
        # get container from data_manager
        # add or remove from container model

    def load_data(self, data):
        def opt(elem): return self.data_manager.add_data(
            elem, idt=self.main_table)
        valid_set = list(map(opt, data))
        return valid_set

    def hover_opciones(self):
        print("Hover Opciones")

    def trigger_opciones(self):
        print("Trigger Opciones")

    def nueva_ventana(self):
        print("Nueva Ventana")
        form_window = WindowForm(self.queue, parent=self)
        self.set_qtimer_read_queue()
        form_window.show()

    def create_chart_container(self, **kwargs):
        model_name = 'data_model'
        chart_window = Container(
            self.data_manager, model_name, parent=self, **kwargs)
        slug = kwargs.get('code')
        self.containers.update({slug: chart_window})
        # add new columnt to tableview ->
        self.new_window_column(**kwargs)
        chart_window.show()

    def new_window_column(self, **kwargs):
        window_data = kwargs.get('code')
        position = kwargs.get('position')
        checkbox = kwargs.get('checkbox')
        model = self.data_manager.get_model(self.main_table)
        model.add_data_column(
            window_data,
            position,
            default_value=False,
            checkable=checkbox,
            role=qt.ItemIsUserCheckable
        )

    def read_queue(self):
        """
        """
        try:
            if not self.queue.empty():
                for i in range(self.queue.qsize()):
                    msg_in = self.queue.get()
                    print("==="*10)
                    [print(k, v) for k, v in msg_in.items()]
                    print("==="*10)
                    command = msg_in.get('command')
                    if command == 'create_container':
                        column_name = msg_in.get('code')
                        if column_name not in self.containers:
                            self.create_chart_container(**msg_in)
                            self._status_update_timer.stop()
                        else:
                            title = "Ventana ya asignada. Por favor vuelve a crear la ventana con otro nombre"
                            md = QErrorMessage()
                            md.setWindowTitle("Error al Crear Contenedor")
                            md.showMessage(title)
                            md.resize(200, 160)
                            md.exec_()
        except Exception as ex:
            print("Error con modulo de escritura de cola")
            raise ex

        # Construir la interfaz

    def set_qtimer_read_queue(self, time_value_ms: int = 100) -> None:
        if not self._status_update_timer:
            self._status_update_timer = QTimer(self)
            self._status_update_timer.setSingleShot(False)
            self._status_update_timer.timeout.connect(self.read_queue)
            self._status_update_timer.start(time_value_ms)
        else:
            return self._status_update_timer.start()


def run_interfaz(lista_values, kwargs):
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    # ex = DragonInterface(loop=loop, **kwargs)
    ex = Gui(list_values, **kwargs)
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == "__main__":
    queue = queue.Queue()
    options = {'queue': queue}
    run_interfaz(list_values, options)
