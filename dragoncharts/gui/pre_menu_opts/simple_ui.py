import sys
import asyncio
import queue
from PyQt5.QtWidgets import (QPlainTextEdit,
                             QPlainTextDocumentLayout,
                             QApplication,
                             QDialog,
                             QWidget,
                             QMainWindow,
                             QPushButton,
                             QMessageBox,
                             QAction,
                             QLineEdit,
                             QGroupBox,
                             QTabWidget,
                             QTableWidget,
                             QTableWidgetItem,
                             QBoxLayout,
                             QVBoxLayout,
                             QGridLayout,
                             QMenuBar,
                             QMenu,
                             QSpinBox,
                             QLabel)

from PyQt5.QtGui import QIcon, QWindow
from PyQt5.QtCore import (QSize, pyqtSignal, pyqtSlot, QTimer)

from layout_wrapper import (BoxLayout,
                            HBoxLayout,
                            VBoxLayout,
                            GridLayout,
                            FormLayout)

from PyQt5.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor

AEventLoop = type(asyncio.get_event_loop())

# Una clase intermedia o bridge para juntar
# los eventloop async


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


# Declarar la creación de la interfaz
# QMainWindow permite crear una clase
# que levanta una ventana
# QWdidget > QMainWindow
#
# http://pyqt.sourceforge.net/Docs/PyQt4/qmainwindow.html#details
"""
Provee un marco de trabajo para construir la app, además existen las clases relacionadas
Además tiene su propio Layout, en el cual se peude agregar QToolBars, QDockWidgets, QMenuBar, QStatusBar
Layout contiene una zona central que puede ser ocupada por cualquier widget

http://pyqt.sourceforge.net/Docs/PyQt4/qmainwindow.html

"""

class Container(QMainWindow):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.width = kwargs.get('width')
        self.height = kwargs.get('height')
        self.setGeometry(0,0, self.width, self.height)
        self.setWindowTitle(kwargs.get('title'))

class WindowForm(QMainWindow):
    form_opts = {'title': "Gráfica GNSS",
                 'command':'create_container',
                 'height': 400,
                 'width': 600}

    def __init__(self, queue, parent=None, *args, **kwargs):
        super().__init__(parent)
        self.queue = queue
        self.setGeometry(0,0, 400, 100)
        self.setWindowTitle("Crear Contenedor de Gráficas")
        self.create_form()

    def create_form(self):
        central_widget = QWidget(self)
        central_widget.setLayout(QGridLayout())
        self.setCentralWidget(central_widget)
        central_layout = central_widget.layout()
        central_layout.setRowStretch(1, 1)
        title_label = QLabel('Nombre de Ventana')
        window_title = QLineEdit(self)
        window_title.setText(self.form_opts.get('title'))
        self.title = window_title
        window_title.textChanged.connect(self.text_change)
        central_layout.addWidget(title_label, 1, 0)
        central_layout.addWidget(window_title, 1, 1)
        size_label = QLabel('Tamaño')
        central_layout.addWidget(size_label, 2, 0, 1, 1)
        w_min = 500
        w_max = 1600
        w_label = QLabel('Ancho [%d,%d]:'%(w_min,w_max))
        width = QSpinBox(self)
        width.setMinimum(w_min)
        width.setMaximum(w_max)
        width.valueChanged.connect(self.w_value_change)
        h_min = 400
        h_max = 1200
        h_label = QLabel('Alto: [%d, %d]' %(h_min, h_max))
        height = QSpinBox(self)
        height.setMinimum(h_min)
        height.setMaximum(h_max)
        height.valueChanged.connect(self.h_value_change)
        central_layout.addWidget(w_label, 3, 0)
        central_layout.addWidget(h_label, 3, 1)
        central_layout.addWidget(width, 4, 0)
        central_layout.addWidget(height, 4, 1)

    def get_opts(self):
        return self.form_opts

    def text_change(self, value):
        self.form_opts.update({'title': value})

    def w_value_change(self, value):
        self.form_opts.update({'width': value})

    def h_value_change(self, value):
        self.form_opts.update({'height': value})

    def closeEvent(self, event):
        self.form_opts.update({'command':'create_container'})
        self.queue.put(self.form_opts)


class Gui(QMainWindow):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        # up -> MenuBar
        # around -> Toolbars
        # bottom -> Status Bar
        self.queue = kwargs.get('queue')
        self._status_update_timer = None
        self.create_menu()

    def create_menu(self):
        menu_bar = self.menuBar()
        # menu != acción
        # Agregar menu
        icon_path = './icons/basic_ui/SVGs/{name}.svg'
        menu_opciones = menu_bar.addMenu('Opciones')
        # send -> self.opt_signal.emit(X)
        # Some standar actions on menu:
        # menu_opciones.hovered.connect(self.hover_opciones)
        menu_opciones.triggered.connect(self.trigger_opciones)
        # icon new_window
        nw_icon = QIcon(icon_path.format(name='Star'))
        nueva_ventana = menu_opciones.addAction(nw_icon, 'Nueva Ventana')
        nueva_ventana.triggered.connect(self.nueva_ventana)
        save_icon = QIcon(icon_path.format(name='Cog'))
        save_settings = menu_opciones.addAction(
            save_icon, 'Guardar Configuración')
        load_icon = QIcon(icon_path.format(name='File'))
        load_settings = menu_opciones.addAction(
            load_icon, 'Cargar Configuración')

    def hover_opciones(self):
        print("Hover Opciones")

    def trigger_opciones(self):
        print("Trigger Opciones")

    def nueva_ventana(self):
        print("Nueva Ventana")
        form_window = WindowForm(self.queue, parent=self)
        self.set_qtimer_read_queue()
        form_window.show()

    def create_chart_container(self, **kwargs):
        chart_window = Container(parent=self, **kwargs)
        chart_window.show()

    def read_queue(self):
        """
        _queue_read :: read the data from queue
        network_ts :: object with list of gnss timeseries

        a {data:X, destiny:'buffer',query:id, page: N, total_pg: M}

        data -> buffer:
        station?
        timestamp?
        dENU
        sigmaENU

        network_ts.add_point_to_station(
                 station, coords, std_coords, timestamp, **karwgs)  ??  ***

        if a.page == a.total:
            self.qtimer.stop()
        """
        try:
            if not self.queue.empty():
                for i in range(self.queue.qsize()):
                    msg_in = self.queue.get()
                    print("==="*10)
                    [print(k,v) for k,v in msg_in.items()]
                    print("==="*10)
                    command = msg_in.get('command')
                    if command == 'create_container':
                        self.create_chart_container(**msg_in)
                        self._status_update_timer.stop()
        except Exception as ex:
            print("Error con modulo de escritura de cola")
            raise ex


        # Construir la interfaz
    def set_qtimer_read_queue(self, time_value_ms: int=100) -> None:
        if not self._status_update_timer:
            self._status_update_timer = QTimer(self)
            self._status_update_timer.setSingleShot(False)
            self._status_update_timer.timeout.connect(self.read_queue)
            self._status_update_timer.start(time_value_ms)
        else:
            return self._status_update_timer.start()


def run_interfaz(kwargs):
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    # ex = DragonInterface(loop=loop, **kwargs)
    ex = Gui(**kwargs)
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == "__main__":
    queue = queue.Queue()
    options = {'queue': queue}
    run_interfaz(options)
