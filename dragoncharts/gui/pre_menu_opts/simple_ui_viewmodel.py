from quamash import QEventLoop, QThreadExecutor
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import (QSize, pyqtSignal, pyqtSlot, QTimer)
from PyQt5.QtGui import QIcon, QWindow
from PyQt5.QtWidgets import (QPlainTextEdit,
                             QPlainTextDocumentLayout,
                             QApplication,
                             QDialog,
                             QWidget,
                             QMainWindow,
                             QPushButton,
                             QMessageBox,
                             QAction,
                             QLineEdit,
                             QGroupBox,
                             QTabWidget,
                             QTableWidget,
                             QTableWidgetItem,
                             QBoxLayout,
                             QVBoxLayout,
                             QGridLayout,
                             QMenuBar,
                             QMenu,
                             QSpinBox,
                             QLabel,
                             QHeaderView)
from PyQt5.QtWidgets import QTableView, QTableWidgetItem

import queue
import asyncio
import sys
from container_table import DataModel
header = ['code', 'station_name', 'lat', 'lon']
values = [
    ('ADBV', 'Adubai', 124, 541),
    ('QURD', 'Queretaro', -45, 67),
    ('MANI', 'Managua', 45, 81),
    ('URYA', 'Haciendo Uruguay', 10, -45)
]
list_values = [dict(zip(header, value)) for value in values]


AEventLoop = type(asyncio.get_event_loop())

# Una clase intermedia o bridge para juntar
# los eventloop async


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


class Gui(QMainWindow):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent)
        # up -> MenuBar
        # around -> Toolbars
        # bottom -> Status Bar
        self.queue = kwargs.get('queue')
        self._status_update_timer = None
        self.datamodel = DataModel(parent=self)
        self.create_menu()
        self.valid_set = self.load_data(list_values)

    def create_menu(self):
        menu_bar = self.menuBar()
        # menu != acción
        # Agregar menu
        icon_path = './icons/basic_ui/SVGs/{name}.svg'
        menu_opciones = menu_bar.addMenu('Opciones')
        # send -> self.opt_signal.emit(X)
        # Some standar actions on menu:
        # menu_opciones.hovered.connect(self.hover_opciones)
        # menu_opciones.triggered.connect(self.trigger_opciones)
        # icon new_window
        nw_icon = QIcon(icon_path.format(name='Star'))
        nueva_ventana = menu_opciones.addAction(nw_icon, 'Nueva Ventana')
        # nueva_ventana.triggered.connect(self.nueva_ventana)
        save_icon = QIcon(icon_path.format(name='Cog'))
        save_settings = menu_opciones.addAction(
            save_icon, 'Guardar Configuración')
        load_icon = QIcon(icon_path.format(name='File'))
        load_settings = menu_opciones.addAction(
            load_icon, 'Cargar Configuración')
        # Table definition
        self.create_table()

    def create_table(self):
        table_widget = QTableView(parent=self)
        horizontal_header = table_widget.horizontalHeader()
        horizontal_header.setSectionResizeMode(QHeaderView.Stretch)
        table_widget.setModel(self.datamodel)
        self.setCentralWidget(table_widget)

    def load_data(self, data):
        model = self.datamodel
        def opt(elem): return model.add_data(elem.get('code'), elem)
        valid_set = list(map(opt, data))
        return valid_set


def run_interfaz(kwargs):
    app = QApplication(sys.argv)
    loop = QEventLoopPlus(app)
    asyncio.set_event_loop(loop)
    # ex = DragonInterface(loop=loop, **kwargs)
    ex = Gui(**kwargs)
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == "__main__":
    queue = queue.Queue()
    options = {'queue': queue}
    run_interfaz(options)
