from PyQt5.QtWidgets import (QBoxLayout,
                             QHBoxLayout,
                             QVBoxLayout,
                             QGridLayout,
                             QFormLayout)


class BoxLayout(QBoxLayout):
    def __init__(self, master, *args, **kwargs):
        # Options
        directions = [QBoxLayout.LeftToRight,
                      QBoxLayout.RightToLeft,
                      QBoxLayout.TopToBottom,
                      QBoxLayout.BottomToTop,
                      QBoxLayout.Down,
                      QBoxLayout.Up]
        direction_value = kwargs.get('direction', 0)
        self.direction = directions[direction_value]
        self.parent = kwargs.get('parent', None)
        super(BoxLayout, self).__init__(self.direction, parent=self.parent)


class HBoxLayout(QHBoxLayout):
    def __init__(self, master, *args, **kwargs):
        # Options
        self.parent = kwargs.get('parent', None)
        print("HBOXLAYOUT->parent", self.parent)
        super(HBoxLayout, self).__init__()


class VBoxLayout(QVBoxLayout):
    def __init__(self, master, *args, **kwargs):
        # Options
        self.parent = kwargs.get('parent', None)
        super(VBoxLayout, self).__init__(parent=self.parent)


class GridLayout(QGridLayout):
    def __init__(self, master, *args, **kwargs):
        # Options
        self.parent = kwargs.get('parent', None)
        super(GridLayout, self).__init__(parent=self.parent)


class FormLayout(QFormLayout):
    def __init__(self, master, *args, **kwargs):
        # Options
        self.parent = kwargs.get('parent', None)
        super(FormLayout, self).__init__(parent=self.parent)
