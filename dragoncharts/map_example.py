import sys
from gui.dbus_map import GuiMap
import asyncio
from PyQt5.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
from networktools.colorprint import gprint, bprint, rprint
from multiprocessing import Manager, Queue

AEventLoop = type(asyncio.get_event_loop())

class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


if __name__ == "__main__":
    def run_map(kwargs):
        #read_queue=
        #write_queue=
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        #ex = DragonInterface(loop=loop, **kwargs)
        gprint("Init DragonInterface")
        ex = GuiMap(**kwargs)
        rprint(ex)
        ex.showMaximized()
        sys.exit(app.exec_())

    idg = 'gui_gnss'

    options_map = {
        'group': ["VALN","TRPD","QTAY"],
        'queue_list': (Queue(),Queue()),
        'chart': 'gnss_map',
        'id_gui': 'GNSS_MAP',
        'type_bus': 'session',
        'module': 'dragoncharts_%s' % idg}

    run_map(options_map)
