from networktools.environment import get_env_variable
import multiprocessing

GUI_WORKERS = int(get_env_variable('GUI_WORKERS'))
GUI_STATIONS_BY_WORKER = int(get_env_variable('GUI_STATIONS_BY_WORKER'))

user = 'geodesia'
host = '10.54.217.15'
gnsocket_port = 6677
nproc = multiprocessing.cpu_count()

SELECT_GROUP = int(get_env_variable('SELECT_GROUP'))

st_dict = dict([
    (0, ['TRPD', 'ATJN', 'VALN', 'CRSC', 'RCSD', 'HSCO']),
    (1, ['AEDA', 'CHYT', 'CTPC', 'FMCO', 'JRGN', 'LSCH']),
    (2, ['MCLA', 'PATH', 'PAZU', 'PCCL', 'PFRJ', 'PVCA']),
    (3, ['QTAY', 'UDAT', 'UTAR', 'UAPE', 'QSCO', 'CGTC'])])


all_codes = [
    "MCLA",
    "VALN",
    "JRGN",
    "UDAT",
    "TRPD",
    "QTAY",
    "CRSC",
    "PFRJ",
    "AEDA",
    "PAZU",
    "PCCL",
    "CHYT",
    "RCSD",
    "PVCA",
    "HSCO",
    "LSCH",
    "UAPE",
    "PATH",
    "ATJN",
    "FMCO",
    "PCMU",
    "VLZL",
    "LLCH",
    "NAVI",
    "ZAPA",
    "QSCO",
    "CGTC",
    "CMRC",
    "ELOA",
    "BING",
    "EMAT",
    "SLM1",
    "CIFU",
    "DGF1",
]


#[all.extend(group) for group in st_dict.values()]

st_dict.update({"ALL": all_codes})

GROUP = st_dict.get(SELECT_GROUP, 0)
COLLECTOR_SOCKET_IP = get_env_variable('COLLECTOR_SOCKET_IP')
COLLECTOR_SOCKET_PORT = int(get_env_variable('COLLECTOR_SOCKET_PORT'))
RDB_SOURCE = get_env_variable('RDB_SOURCE')
RDB_ENU = get_env_variable('RDB_ENU')
DATAWORK_SOCKET_IP = get_env_variable('DATAWORK_SOCKET_IP')
DATAWORK_SOCKET_PORT = int(get_env_variable('DATAWORK_SOCKET_PORT'))
