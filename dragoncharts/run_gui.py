# Standar library
import socket
from settings import GUI_STATIONS_BY_WORKER, \
    GUI_WORKERS, \
    GROUP, \
    st_dict
from settings import (RDB_SOURCE, RDB_ENU)
from settings import (COLLECTOR_SOCKET_IP, COLLECTOR_SOCKET_PORT)
from settings import (DATAWORK_SOCKET_IP, DATAWORK_SOCKET_PORT)

import sys
import argparse
import asyncio
import functools
import concurrent.futures as cf
import ujson as json

# Modules
from multiprocessing import Manager, Queue
from PyQt5.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
from PyQt5.QtCore import QObject, pyqtSignal, QProcess

# Contrib modules
from gnsocket.gn_socket import GNCSocket
from gnsocket.socket_client import GNCSocketClient


from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

# Gui Module
from gui.dbus_dt import Gui
#from gui.dbus_map import GuiMap

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps


AEventLoop = type(asyncio.get_event_loop())


class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


host_server = socket.gethostbyname(socket.gethostname())


# Enables a socket to listen messages and send some data


# Arg parser to select particular group
parser = argparse.ArgumentParser(description="Obtener parámetros de operación")

parser.add_argument('--group',
                    help="Select the group of stations {0, 1, 2, 3, ALL}",
                    )

parser.add_argument('--id',
                    type=str,
                    help="Set id for this GUI",)


if __name__ == "__main__":
    rprint("El diccionario de grupos")
    gprint(st_dict)
    loop = asyncio.get_event_loop()
    workers = 2
    tsleep = 2
    executor = cf.ProcessPoolExecutor(workers)
    manager = Manager()
    q_send = manager.Queue()
    q_recv = manager.Queue()
    q_read_map = manager.Queue()
    q_write_map = manager.Queue()
    address = (DATAWORK_SOCKET_IP, DATAWORK_SOCKET_PORT)

    args = parser.parse_args()
    group = st_dict[0]

    if args.group:
        if args.group.isdigit():
            group = int(args.group)
        else:
            group = args.group
    group = st_dict.get(group)
    # Add ALL to group because on Datawork is a condition to add the stations that
    # doesn't exist yet
    if args.group == 'ALL':
        group.append('ALL')
    idg = 'gui_gnss'
    if args.id:
        idg = args.id

    est_by_proc = GUI_STATIONS_BY_WORKER

    def run_interfaz(kwargs):
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        #ex = DragonInterface(loop=loop, **kwargs)
        gprint("Init DragonInterface")
        ex = Gui(**kwargs)
        rprint(ex)
        ex.showMaximized()
        sys.exit(app.exec_())

    # Task to receive on socket messages from outside

    queue_list = (q_send, q_recv)
    socket_client = GNCSocketClient(q_send, q_recv, address=address)

    options = {
        'group': group,
        'queue_list': queue_list,
        'chart': 'gnss_graph',
        'id_gui': idg,
        'type_bus': 'session',
        'module': 'dragoncharts_%s' % idg}

    tasks = []

    sockettask = loop.run_in_executor(
        executor,
        socket_client.socket_task
    )
    tasks.append(sockettask)

    guitask = loop.run_in_executor(
        executor,
        functools.partial(
            run_interfaz,
            options
        )
    )

    tasks.append(guitask)

    """
    maptask = loop.run_in_executor(
        executor,
        functools.partial(
            run_map,
            options_map
        )
    )

    tasks.append(maptask)
    """

    rprint("Running GUI and Map Gui")

    # activate all tasks
    loop.run_until_complete(
        asyncio.gather(
            *tasks
        )
    )
