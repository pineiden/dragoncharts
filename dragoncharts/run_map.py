# Standar library
import sys
import argparse
import asyncio
import functools
import concurrent.futures as cf
import simplejson as json

# Modules
from multiprocessing import Manager, Queue
from PyQt5.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
from PyQt5.QtCore import QObject, pyqtSignal, QProcess

# Contrib modules 
from gnsocket.gn_socket import GNCSocket
from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

# Gui Module
#from gui.dbus_dt import Gui
from gui.dbus_map import GuiMap

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps


AEventLoop = type(asyncio.get_event_loop())

class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


# Enables a socket to listen messages and send some data


# Arg parser to select particular group
parser=argparse.ArgumentParser(description="Obtener parámetros de operación")

parser.add_argument('--group',
                    help="Select the group of stations {0, 1, 2, 3, ALL}",
                    )

parser.add_argument('--id',
                    type=str,
                    help="Set id for this GUI",)


from settings import GUI_STATIONS_BY_WORKER, \
    GUI_WORKERS, \
    GROUP, \
    st_dict


from gnsocket.gn_socket import GNCSocket


if __name__ == "__main__":
    rprint("El diccionario de grupos")
    gprint(st_dict)
    loop = asyncio.get_event_loop()
    workers = 2
    tsleep = 2
    executor = cf.ProcessPoolExecutor(workers)
    manager = Manager()
    q_send = manager.Queue()
    q_recv = manager.Queue()
    q_read_map = manager.Queue()
    q_write_map = manager.Queue()
    queue_list = (q_send, q_recv)
    address = ('localhost', 6666)
    gs = GNCSocket(mode='client')
    gs.set_address(address)

    args = parser.parse_args()
    group = st_dict[0]

    if args.group:
        if args.group.isdigit():
            group = int(args.group)
            if group in st_dict.keys():
                group = st_dict[group]
        else:
            if args.group in st_dict.keys():
                group = st_dict[args.group]

    idg = 'gui_gnss'
    if args.id:
        idg = args.id

    est_by_proc = GUI_STATIONS_BY_WORKER
    
    def run_map(kwargs):
        #read_queue=
        #write_queue=
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        #ex = DragonInterface(loop=loop, **kwargs)
        gprint("Init DragonInterface")
        ex = GuiMap(**kwargs)
        rprint(ex)
        ex.showMaximized()
        sys.exit(app.exec_())

    # Socket managment

    # QT Signal in shared memory

    service = 'csn.gui.coord'
    interface = "%s.interface" %service
    variable = 'message'

    dbus_options = {
        'service': service,
        'interface': interface,
        'variable': variable,
        'path':'/data'
    }

    options_map = {
        'group': group,
        'queue_list': queue_list,
        'chart': 'gnss_map',
        'id_gui': 'GNSS_MAP',
        'type_bus': 'session',
        'module': 'dragoncharts_%s' % idg,
        'dbus_ref': dbus_options
    }

    tasks = []

    # Task to receive on socket messages from outside

    maptask = loop.run_in_executor(
        executor,
        functools.partial(
            run_map,
            options_map
        )
    )

    tasks.append(maptask)

    rprint("Running GUI and Map Gui")

    # activate all tasks
    loop.run_until_complete(
        asyncio.gather(
            *tasks
        )
    )
