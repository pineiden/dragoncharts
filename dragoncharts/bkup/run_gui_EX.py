# Standar library
import sys
import argparse
import asyncio
import functools
import concurrent.futures as cf
import simplejson as json

# Modules
from multiprocessing import Manager, Queue
from PyQt5.QtWidgets import QApplication
from quamash import QEventLoop, QThreadExecutor
from PyQt5.QtCore import QObject, pyqtSignal, QProcess

# Contrib modules 
from gnsocket.gn_socket import GNCSocket
from tasktools.taskloop import coromask, renew, simple_fargs
from networktools.colorprint import gprint, bprint, rprint

# Gui Module
from qtgui.interface import DragonInterface

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps


AEventLoop = type(asyncio.get_event_loop())

class QEventLoopPlus(QEventLoop, AEventLoop):
    pass


# Enables a socket to listen messages and send some data


# Arg parser to select particular group
parser=argparse.ArgumentParser(description="Obtener parámetros de operación")

parser.add_argument('--group',
                    type=str,
                    help="Select the group of stations {0,1,ALL}",)

parser.add_argument('--id',
                    type=str,
                    help="Set id for this GUI",)


from settings import GUI_STATIONS_BY_WORKER, \
    GUI_WORKERS, \
    GROUP, \
    st_dict


from gnsocket.gn_socket import GNCSocket


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    workers = 3
    tsleep = 2
    executor = cf.ProcessPoolExecutor(workers)
    manager = Manager()
    q_send = manager.Queue()
    q_recv = manager.Queue()
    queue_list = (q_send, q_recv)
    address = ('localhost', 6666)
    gs = GNCSocket(mode='client')
    gs.set_address(address)

    args = parser.parse_args()
    group = st_dict["0"]
    if args.group:
        group = st_dict[args.group]
    idg = 'gui_gnss'
    if args.id:
        idg = args.id

    est_by_proc = GUI_STATIONS_BY_WORKER
    
    def run_interfaz(kwargs):
        app = QApplication(sys.argv)
        loop = QEventLoopPlus(app)
        asyncio.set_event_loop(loop)
        ex = DragonInterface(**kwargs)
        ex.showMaximized()
        sys.exit(app.exec_())
        
    # Socket managment

    # QT Signal in shared memory

    async def sock_write(queue_t2n, idc):
        queue=queue_t2n
        await asyncio.sleep(tsleep)
        try:
            print("Check queue from GUI is empty")
            print(queue)
            print(queue.empty())
            if not queue.empty():
                for i in range(queue.qsize()):
                    msg_in = queue.get()
                    bprint("Recibido desde gui : %s" %msg_in)
                    await gs.send_msg(gns_dumps(msg_in), idc)
                await gs.send_msg("<END>", idc)
            else:
                pass
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec
    # socket communication terminal to engine
    async def sock_read(queue_n2t, idc):
        queue=queue_n2t
        msg_from_engine=[]
        await asyncio.sleep(tsleep)
        try:
            print("Check if there are messages from source")
            # read queue is answer from msg sended
            datagram = await gs.recv_msg(idc)
            bprint("Recibido desde socket server %s " %datagram)
            if not datagram == '' and \
               datagram != "<END>":
                queue.put(gns_loads(datagram))
        except Exception as exec:
            raise exec


    def socket_task(queue_list):
        # client socket
        print("Socket tasks: (writer, reader)")
        loop=asyncio.get_event_loop()
        gs.set_loop(loop)
        async def socket_io(queue_list):
            queue_read=queue_list[0]
            queue_write=queue_list[1]
            idc = await gs.create_client()
            #First time welcome
            #welcome={'command':'greetings', 'args':["Welcome to socket"]}
            #rprint(welcome)
            #await gs.send_msg(welcome)
            #await gs.send_msg("<END>")
            #task reader
            try:
                args=[queue_read, idc]
                task=loop.create_task(
                    coromask(
                        sock_read,
                        args,
                        simple_fargs)
                )
                task.add_done_callback(
                    functools.partial(
                        renew,
                        task,
                        sock_read,
                        simple_fargs)
                )
                args=[queue_write, idc]
                #task write
                task=loop.create_task(
                    coromask(
                        sock_write,
                        args,
                        simple_fargs)
                )
                task.add_done_callback(
                    functools.partial(
                        renew,
                        task,
                        sock_write,
                        simple_fargs)
                )
            except Exception as exec:
                raise exec

        ########
        # Insert a coroutine with reader and writer tasks

        async def activate_sock(queue_list):
            await socket_io(queue_list)
            return idc

        future1 = loop.create_task(activate_sock(queue_list))

        print("Loop is running?", loop.is_running())
        if not loop.is_running():
            loop.run_forever()

    options = {
        'group': group,
        'queue_list': queue_list,
        'chart': 'gnss_graph',
        'id_gui': idg,
        'type_bus': 'session',
        'module': 'dragoncharts_%s' % idg}

    tasks = []

    # Task to receive on socket messages from outside

    sockettask = loop.run_in_executor(
        executor,
        functools.partial(socket_task, queue_list)
    )
    tasks.append(sockettask)

    guitask = loop.run_in_executor(
        executor,
        functools.partial(
            run_interfaz,
            options
        )
    )

    tasks.append(guitask)

    # activate all tasks
    loop.run_until_complete(
        asyncio.gather(
            *tasks
        )
    )
