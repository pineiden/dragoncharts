import tkinter as tk
from tkinter import ttk
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from gnss_traces.gnss_traces import PlotTraces


import math
import random
from tkinter.scrolledtext import ScrolledText

import time

from networktools.geo import ecef2llh, rad2deg, get_from_ecef

# Call GNSS Traces

import matplotlib.pyplot as plt
from gnss_traces.gnss_traces import PlotTraces
import numpy as np

##

class DragonInterface(tk.Frame):
    def __init__(self, master=None, *args, **kwargs):
        super().__init__(master)
        if 'process_queue' in kwargs.keys():
            self.process_queue=kwargs['process_queue']
        if 'stations_queue' in kwargs.keys():
            self.stations_queue=kwargs['stations_queue']
        if 'size' in kwargs.keys():
            self.size=kwargs['size']
        if 'nen' in kwargs.keys():
            self.nen=kwargs['nen']
        self.stations={}
        self.notebook_stations={}
        self.assign_stations={}
        self.position={}
        self.ready_assing=[]
        self.guide={}
        self.pack()
        self.build_interface()
        #self.add_chart('TEST')
        self.count=0
        self.count_st=0
        self.root=master
        self.selected_sta=[]
        time.sleep(10)

        self.chart=None
        self.chart_updates()

    def add_process_queue(self, process_queue):
        self.process_queue=process_queue

    def read_from_queue(self):
        self.data_text.insert(tk.INSERT,"Reading data....\n") 
        self.data_text.insert(tk.INSERT,"Queue %s\n" % self.process_queue) 
        #q=self.process_queue
        if self.count==120:
            #self.data_text.clear()
            self.count=0

        try:
            if not self.process_queue.empty():
                self.data_text.insert(tk.INSERT,"New Data from Datawork, count %s --->\n" %self.count) 
                for i in range(self.process_queue.qsize()):
                    msg=self.process_queue.get()
                    #self.data_text.insert(tk.INSERT, "%s, %s ->  %s" % (msg['station_name'],msg['DT_GEN'],msg))
                    self.data_text.insert(tk.INSERT,"\n")
                    if self.chart and msg['station_name'] in self.selected_sta:
                        #self.stations_print("Adding to chart %s" %msg)
                        self.chart.add_point(msg)
                    self.process_queue.task_done()
                self.data_text.insert(tk.INSERT,"====[END]====\n")
                self.data_text.insert(tk.INSERT,"\n")
                if self.chart:
                    self.stations_print("Pre refresh traces")
                    try:
                        self.chart.refresh_traces()
                        self.canvas.draw()
                    except Exception as e:
                        self.stations_print("Error en cargar plt %s" %s)
                        raise e
                self.count+=1
        except Exception as e:
            self.stations_print("Error en punto 7min %s" %e)
            raise e
    
    def read_stations_queue(self):
        if not self.stations_queue.empty():
            self.data_text.insert(tk.INSERT,"New Data from Datawork, count %s --->\n" %self.count) 
            for i in range(self.stations_queue.qsize()):
                msg=self.stations_queue.get()
                command=msg['command']
                data=msg['data']
                if command=='station':
                    self.stations.update(data)
                    station=tuple(data.items())
                    self.guide.update({station[0][1]['code']:station[0][0]})
                    self.stations_text.insert(tk.INSERT,"========\n")
                    station_ids=list(msg.keys())[0]
                    self.stations_text.insert(tk.INSERT,station_ids)
                    self.stations_text.insert(tk.INSERT,"\n")
                    self.stations_text.insert(tk.INSERT,"========\n")
                    self.stations_text.insert(tk.INSERT, data)
                elif command=='position':
                    self.position.update(data)
                    self.stations_text.insert(tk.INSERT,"========\n")
                    self.stations_text.insert(tk.INSERT,data)
                    self.stations_text.insert(tk.INSERT,"\n")
                    self.stations_text.insert(tk.INSERT,"========\n")
                self.stations_queue.task_done()
    
            self.stations_text.insert(tk.INSERT,"\n")
            self.stations_text.insert(tk.INSERT,"====[END]====\n")
            self.stations_text.insert(tk.INSERT,"\n")
            self.count_st+=1
            #self.add_chart('GNSS')


    def stations_print(self, text):
        self.stations_text.insert(tk.INSERT,"#"*20+"\n")
        self.stations_text.insert(tk.INSERT,text)
        self.stations_text.insert(tk.INSERT,"\n")
        self.stations_text.insert(tk.INSERT,"#"*20+"\n")

    def build_interface(self):

        self.button_frame=ttk.Frame(self)
        self.button_frame.pack(side='top',fill=tk.BOTH, expand=True)

        self.update_button=tk.Button(self.button_frame, command=self.update_button_st)
        self.update_button["text"]="Update Stations"
        self.update_button.pack(side="left")

        self.data_button=tk.Button(self.button_frame, command=self.data_button_rn)
        self.data_button["text"]="Add data"
        self.data_button.pack(side="left")

        self.data_button=tk.Button(self.button_frame, command=self.chart_button)
        self.data_button["text"]="Load Chart"
        self.data_button.pack(side="left")


        self.notebook= ttk.Notebook(self,
                                    height=self.size['height']-60,
                                    width=self.size['width']-60, padding=10)
        self.f1=ttk.Frame(self.notebook)
        self.notebook.add(self.f1, text="Charts")
        self.notebook.pack(side="bottom")

        time.sleep(.2)

        self.f2=ttk.Frame(self.notebook)
        self.stations_text=ScrolledText(self.f2)
        self.stations_text.pack(expand=1, fill="both")
        self.notebook.add(self.f2, text="Station List")
        self.notebook.pack(side='bottom')

        self.f3=ttk.Frame(self.notebook)
        self.data_text=ScrolledText(self.f3)
        self.data_text.pack(expand=1, fill="both")
        self.notebook.add(self.f3, text="Data from Stations")
        self.notebook.pack(side='bottom')

    def new_notebook_chart(self):


        return idn


    def set_idn(self, uin=3):
        """
        Defines a new id for stations, check if exists
        """
        idn = my_random_string(uin)
        while True:
            if idn not in self.idn:
                self.ido.append(idn)
                break
            else:
                ido = my_random_string(uin)
        return ido



    def chart_updates(self):
        self.read_from_queue()
        self.read_stations_queue()
        # Adjust axis

        #read queues and reload charts

        self.root.after(3000, self.chart_updates)

    def update_button_st(self):
        print("Update stations list")

    def data_button_rn(self):
        ids='TEST'
        last=self.stations[ids]['x'][-1]
        self.stations[ids]['x'].append(last+1)
        new_y=random.randint(-10,10)
        self.stations[ids]['y'].append(new_y)
        #print(last+1,new_y)
        #print(self.stations[ids])
        self.stations[ids]['chart'].plot(self.stations[ids]['x'],self.stations[ids]['y'])
        self.stations[ids]['canvas'].draw()
        self.read_from_queue()


    def chart_button(self):
        ids="GNSS"
        self.stations_print("Loading chart %s" %ids)
        self.add_chart(ids)

    def add_chart(self, ids):
        #time.sleep(1)
        #self.stations.update({ids:{'x':[1,2,3,4,5,6,7,8],
        #                           'y':[5,6,1,3,8,9,3,5]}})

        #f=Figure(figsize=(3,2), dpi=100)
        #a=f.add_subplot(111)
        #a.plot(self.stations[ids]['x'],self.stations[ids]['y'])

        self.stations_print("Posiciones ....%s" %self.position)
        self.stations_print("Guia ....%s" %self.guide)

        sta_ref=[]

        try:
            stations=["VALN","QTAY","AEDA","ATJN"]
            #stations=["VALN"]

            self.selected_sta=stations
            self.stations_print("Selected stations ....%s" %stations)
            self.stations_print("Define new lat lon" )

            lon=[]
            lat=[]

            self.stations_print("Selected stations ....%s" %stations)


            for s in stations:
                ids=self.guide[s]
                self.stations_print("IDS ....%s" %ids)
                P=self.position[ids]
                self.stations_print("This position ....%s" %P)
                llh=P['llh']
                self.stations_print("LLH ....%s" %llh)
                new_lat=llh['lat']
                new_lon=llh['lon']
                lon.append(new_lon)
                lat.append(new_lat)

            self.stations_print("Array lon %s" %lon)
            self.stations_print("Array lat %s" %lat)

            sta_ref=[lon,lat]

            self.stations_print(sta_ref)
        except Exception as e:
            self.stations_print("Error pasar posiciones %s" %e)
            raise e

        try:
            self.stations_print("Creating chart %s" %sta_ref)
            f=PlotTraces(stations, sta_ref,
                         data_buffer=1200,
                         display_window=600,
                         s_rate=1,
                         fnp=self.stations_print)
            self.chart=f
            self.stations_print("Plot ok")

            canvas=FigureCanvasTkAgg(f.fig,master=self.f1)
            self.stations_print("Pre show")
            canvas.show()
            #self.stations[ids].update({'chart':f,'canvas':canvas})
            canvas.get_tk_widget().pack(side="top")
            self.canvas=canvas

        except Exception as e:
            self.stations_print("Error en %s" %e)
            raise e

    

