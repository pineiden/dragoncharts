import tkinter as tk
from interface import DragonInterface

if __name__ == "__main__":
    root=tk.Tk()
    kwargs={}
    kwargs.update({'size':{'width':root.winfo_screenwidth(), 'height':root.winfo_screenheight()}})
    app=DragonInterface(master=root,**kwargs)
    app.mainloop()
