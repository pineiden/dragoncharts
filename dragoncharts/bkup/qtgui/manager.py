import sys


from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QPlainTextDocumentLayout
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem

from networktools.colorprint import gprint, bprint, rprint

from PyQt5.QtCore import pyqtSlot

from functools import partial

from networktools.library import my_random_string

import copy

import queue

from qtwidgets import (GuiQWidget,
                        ChartWidget,
                        SinChartWidget,
                        DynamicChartWidget,
                        GNSSChartWidget,
                        GNSSGraphWidget)

class QTManager:
    def __init__(self, *args, **kwargs):
        # contains the stations reference information
        self.stations={}
        # contains the assigned stations
        self.assign_stations={}
        # contains the reference position of stations in ECEF and Lat Lon Height
        self.position={}
        self.ready_assign=[]
        # contains the keys,values where keys are stations code and values the ids
        self.guide={}
        # Counter for data arrived and stations or position data
        self.count=0
        self.count_st=0
        # A list of selected stations
        self.selected_sta=[]

        #Hierarchical GUI::::>>>

        # A group of qwidgets, keys:{instance, id_layouts}
        self.windows={}
        #A group of layouts, keys:{instance, id_widgets}
        self.layouts={}
        # A group of tab sets,keys {instance, id_tabs}
        self.tab_sets={}
        # A group'of tabs, keys {instance, id_child_widget, type_child_widget}
        self.tabs={}
        # A group of textareas, keys {}
        self.text_areas={}
        # A group of charts, keys {}
        self.charts={}

        # End sets

        #Ids list:
        self.idn=[]#window
        self.idl=[]#layout
        self.idts=[]#tab_set
        self.idt=[]#tab
        self.idtext=[]#textarea
        self.idchart=[]#chart
        # END

        # Set layout type:
        self.type_layouts={
            'grid': QGridLayout,
            'box': QBoxLayout,
            'vertical': QVBoxLayout,
            'horizontal': QHBoxLayout,
            'form': QFormLayout,
        }
        #
        self.empty_widgets={
            'generic':[],
            'button':[],
            'tab_set':[],
            'textfield':[],
            'textarea':[],
            'checkbox':[],
            'combobox':[],
            'selector':[],
            'chart':[],
            'sin_chart':[],
            'dyn_chart':[],
            'gnss_chart':[],
            'gnss_graph':[],
        }
        # can be only filled with **kwargs?
        self.base_widgets={
            'generic': GuiQWidget,
            'button': QPushButton,
            'tab_set': QTabWidget,
            'textarea': QPlainTextEdit,
            'textfield': QLineEdit,
            'chart': GuiQWidget,#Add chart widgetclass
            'sin_chart': SinChartWidget,
            'dyn_chart': DynamicChartWidget,
            'gnss_chart': GNSSChartWidget,
            'gnss_graph': GNSSGraphWidget
        }

        self.create_widgets={
            'tab_set':self.create_tabset,
            'textarea':self.create_text,
            'chart':self.create_chart
        }

        #
        self.log_text=None
        self.log_queue=queue.Queue()


    """
    Generate Ids sections
    """

    def set_idn(self, uin=3):
        """
        Defines a new id for new windows with charts
        """
        idn = my_random_string(uin)
        while True:
            if idn not in self.idn:
                self.idn.append(idn)
                break
            else:
                idn = my_random_string(uin)
        return idn

    def set_idl(self, uin=3):
        """
        Defines a new id for new windows with charts
        """
        idl = my_random_string(uin)
        while True:
            if idl not in self.idl:
                self.idl.append(idl)
                break
            else:
                idl = my_random_string(uin)
        return idl

    def set_idts(self, uin=3):
        """
        Defines a new id for new tab set
        """
        idts = my_random_string(uin)
        while True:
            if idts not in self.idts:
                self.idts.append(idts)
                break
            else:
                idts = my_random_string(uin)
        return idts


    def set_idt(self, uin=3):
        """
        Defines a new id for new tab
        """
        idt = my_random_string(uin)
        while True:
            if idt not in self.idt:
                self.idt.append(idt)
                break
            else:
                idt = my_random_string(uin)
        return idt


    def set_idtext(self, uin=3):
        """
        Defines a new id for new textareas
        """
        idtext = my_random_string(uin)
        while True:
            if idtext not in self.idtext:
                self.idtext.append(idtext)
                break
            else:
                idtext = my_random_string(uin)
        return idtext


    def set_idchart(self, uin=3):
        """
        Defines a new id for new chart
        """
        idchart = my_random_string(uin)
        while True:
            if idchart not in self.idchart:
                self.idchart.append(idchart)
                break
            else:
                idchart = my_random_string(uin)
        return idchart

    """
    END

    NOW:

    Create Window -> Tab Group -> Tab
    """

    def read_queue(self):
        queue=self.log_queue
        list_msg=[]
        if not queue.empty():
            for q in range(queue.qsize()):
                m=queue.get()
                list_msg.append(m)
                self.print_log(m)
                queue.task_done()
                if m is None:
                    break
        return list_msg


    def print_text(self, idtext, msg):
        #print(self.text_areas)
        log_textarea=self.text_areas[idtext]['instance']
        log_textarea.insertPlainText("%s\n" %msg)

    def print_data(self, msg):
        idtext=self.log_data
        if idtext:
            self.print_text(idtext, msg)
        else:
            print(msg)
            self.log_queue.put(msg)


    def print_log(self, msg):
        idtext=self.log_text
        if idtext:
            self.print_text(idtext, msg)
        else:
            print(msg)
            self.log_queue.put(msg)

    def create_window(self, **options):
        idn=self.set_idn()
        fmsg=None
        if 'msg' in options.keys():
            fmsg="Crear ventana %s con idn %s" %(options['msg'],idn)
        else:
            fmsg="Crear ventana con idn %s" %(idn)
        self.statusBar().showMessage(fmsg)
        self.print_log(fmsg)
        #exists log text
        #Now create windows QWidget
        window=QWidget(**options)
        self.windows.update({idn:{'instance':window, 'id_layout':[]}})
        return idn

    def create_layout(self, idn, *args,**options):
        idl=self.set_idl()

        #select container
        if idn is None:
            window=self.main_widget
            idnx='Main Window'
        else:
            window=self.windows[idn]['instance']

        #show message
        fmsg=None
        if 'msg' in options.keys():
            fmsg="Crear layout %s código %s, en ventana idn %s" %(options['msg'], idl, idn)
        else:
            fmsg="Crear layout %s en ventana idn %s" %(idl, idn)
        self.print_log(fmsg)
        self.statusBar().showMessage(fmsg)

        # assign layout to container
        options.update({'parent':window})
        layout_type='box'
        if 'layout_type' in options.keys():
            layout_type=options['layout_type']
            del options['layout_type']

        print(layout_type)
        new_layout=self.type_layouts[layout_type](*args,**options)

        if idn is None:
            self.main_layout=new_layout

        ew=copy.deepcopy(self.empty_widgets)
        self.layouts.update({idl:{'instance':new_layout,'id_widgets':ew}})
        if idn:
            self.windows[idn]['id_layouts']=idl
        else:
            self.windows[0]={}
            self.windows[0].update({'id_layouts':idl})
        return idl


    def create_tabset(self, idl, *args, **options):
        idts=self.set_idts()
        fmsg=None
        #Control message
        if 'msg' in options.keys():
            fmsg="Crear tabset %s código %s, en ventana layout %s" %(options['msg'], idts,idl)
        else:
            fmsg="Crear tabset %s en ventana idts %s" %(idts, idl)
        self.print_log(fmsg)
        self.statusBar().showMessage(fmsg)
        #End control message

        if idl is None:
            layout=self.main_layout
            idl=0
        else:
            layout=self.layouts[idl]['instance']

        options.update({"widget":'tab_set'})
        tabset=self.addWidget2Layout(layout, *args, **options)

        if idl is None:
            self.main_tab_set=tabset

        layout.addWidget(tabset)
        self.tab_sets.update({idts:{'instance':tabset,'id_tabs':[]}})
        self.layouts[idl]['id_widgets']['tab_set'].append(idts)
        return idts

    def create_tab(self, idts, *args, **options):
        idt=self.set_idt()
        #Control message
        fmsg=None
        if 'msg' in options.keys():
            fmsg="Add tab %s, idt  %s to tabset %s" %(options['msg'], idt, idts)
        else:
            fmsg="Add tab id %s to tabset idts %s" %(idt, idts)
        self.print_log(fmsg)
        self.statusBar().showMessage(fmsg)
        #End control message

        if idts is None:
            tab_set=self.main_tab_set
        else:
            tab_set=self.tab_sets[idts]['instance']

        type_widget='generic'
        if 'widget' in options.keys():
            type_widget=options['widget']

        name='Tab'
        if 'name' in options.keys():
            name=options['name']
            del options['name']
        
        new_tab=self.base_widgets[type_widget](*args, **options)
        print(new_tab)
        tab_set.addTab(new_tab, name)

        options.update({'parent':new_tab})
        layout=self.addLayout2Widget(new_tab, *args, **options)
        ew=copy.deepcopy(self.empty_widgets)
        self.tabs.update({idt:{'instance':new_tab,'layout':layout,'id_widgets':ew}})
        self.tab_sets[idts]['id_tabs'].append(idt)
        return idt

    def addLayout2Widget(self, widget, *args ,**options):
        #select layout
        layout_type='box'
        if 'layout_type' in options.keys():
            layout_type=options['layout_type']
            del options['layout_type']
        new_layout=self.type_layouts[layout_type](*args,**options)
        #widget.addLayout(new_layout)
        return new_layout

    def addWidget2Layout(self, layout, *args, **options):
        type_widget='generic'
        if 'widget' in options.keys():
            type_widget=options['widget']
            del options['widget']
        # widget instance
        new_widget=self.base_widgets[type_widget](*args,**options)
        layout.addWidget(new_widget)
        return new_widget        

    def create_text(self, idt, *args, **options):
        idtext=self.set_idtext()
        #Control message
        fmsg=None
        if 'msg' in options.keys():
            fmsg="Add textarea %s, idtext  %s to tab %s" %(options['msg'], idtext, idt)
        else:
            fmsg="Add textarea idtext %s to tab idts %s" %(idtext, idt)
        self.print_log(fmsg)
        self.statusBar().showMessage(fmsg)
        #End control message
        options.update({'widget':'textarea'})
        #Obtain tab layout
        layout=self.tabs[idt]['layout']
        ta_ins=self.addWidget2Layout(layout, *args,**options)
        self.text_areas.update({idtext:{'instance':ta_ins}})
        self.tabs[idt]['id_widgets']['textarea'].append(idtext)
        #self.log_text=idtext
        return idtext

    def create_chart(self, idt,*args,  **options):
        idchart=self.set_idchart()
        #Control message
        fmsg=None
        if 'msg' in options.keys():
            fmsg="Add Chart %s, idchart  %s to tab %s" %(options['msg'], idchart, idt)
        else:
            fmsg="Add Chart %s to tabset idts %s" %(idchart, idt)
        self.print_log(fmsg)
        self.statusBar().showMessage(fmsg)
        #End control message

        #Obtain tab layout
        layout=self.tabs[idt]['layout']
        chart='chart'
        if 'widget' in options.keys():
            chart=options['widget']

        #fig->chart_inst
        chart_ins=self.addMultiWidget2Layout(layout, *args, **options)

        self.charts.update({idchart:{'instance':chart_ins}})
        #gprint("Tab names %s"%self.tabs[idt]['id_widgets'])
        self.tabs[idt]['id_widgets'][chart].append(idchart)
        return idchart



    def addMultiWidget2Layout(self, layout, *args, **options):
        type_widget='generic'
        if 'widget' in options.keys():
            type_widget=options['widget']
            del options['widget']
        # widget instance
        new_multi_widget=self.base_widgets[type_widget](*args,**options)
        stations=args[0]
        positions=args[1]
        for k,v in enumerate(stations):
            print("Positions %s" %v)
            print(positions[k])
            new_multi_widget.add_station(v, (positions[k][0],
                                                     positions[k][1]))
        new_multi_widget.create_canvas()
        parent_window=self
        #First chart -> main tab
        if new_multi_widget.least_stations:
            print(new_multi_widget.widget_set)
            layout.addWidget(new_multi_widget.widget_set[0])
            for new_widget in new_multi_widget.widget_set[1::]:
                self.print_log("New Window with %s" %new_widget)
                #create new QWidget
                new_window=QMainWindow(parent=parent_window)
                new_window.setWindowTitle("Gráficos GNSS")
                opt={'parent':new_window}
                new_wd_id=self.create_window(**opt)
                args=[QBoxLayout.LeftToRight]
                opt2=dict(layout_type='box')
                new_layout_id=self.create_layout(new_wd_id, *args,**opt2)
                new_window.showMaximized()
                self.layouts[new_layout_id]['instance'].addWidget(new_widget)

        self.chart_manager=new_multi_widget
        return new_multi_widget
