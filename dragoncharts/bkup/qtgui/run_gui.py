import sys
from PyQt5.QtWidgets import QApplication
from interface import DragonInterface
import queue

if __name__=='__main__':
    app=QApplication(sys.argv)
    queue_process=queue.Queue()
    queue_stations=queue.Queue()
    kwargs=dict(
        process_queue=queue_process,
        stations_queue=queue_stations,
        group='gui'
        )
    ex=DragonInterface(**kwargs)
    #ex.showFullScreen()
    ex.showMaximized()
    sys.exit(app.exec_())
