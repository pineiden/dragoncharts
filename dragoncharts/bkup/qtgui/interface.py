import sys

from PyQt5.QtWidgets import QPlainTextEdit
from PyQt5.QtWidgets import QPlainTextDocumentLayout
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QBoxLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QTabWidget
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem


from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtCore import QTimer

from functools import partial

from networktools.library import my_random_string

import time
import copy

import queue

import simplejson as json

from manager import QTManager
from networktools.dbs.amqp import AMQPData
from networktools.dbs.rethinkdb import RethinkDB

import asyncio
import rethinkdb as rdb
from datetime import datetime as dt
from networktools.colorprint import gprint, bprint, rprint
from networktools.time import get_datetime_di
from networktools.ipc.dbus import GatherDBusAdaptor, GatherDBusInterface, GatherSignal


from quamash import QEventLoop, QThreadExecutor

from tasktools.taskloop import coromask, renew, simple_fargs, renew_quamash

import functools

from PyQt5.QtCore import pyqtSlot, Q_CLASSINFO
from PyQt5 import QtCore, QtDBus

import queue

# Async Socket
from gnsocket.gn_socket import GNCSocket

class DragonInterface(QMainWindow, QTManager):
    signal=pyqtSignal(str)
    message=pyqtSignal(dict)
    def __init__(self, master=None,*args, **kwargs):
        super().__init__(master)
        self.path="/data"
        if 'path' in kwargs.keys():
            self.path=kwargs['path']
        self.service = "csn.atlas.gather"
        if 'service' in kwargs.keys():
            self.service=kwargs['service']
        self.interface="%s.interface" %self.service
        self.variable = "signal "
        if 'variable' in kwargs.keys():
            self.variable=kwargs['variable']
        if 'gui_queue' in kwargs.keys():
            self.gui_queue=kwargs['gui_queue']
        if 'size' in kwargs.keys():
            self.size=kwargs['size']
        if 'nen' in kwargs.keys():
            self.nen=kwargs['nen']
        self.chart_type='sin_chart'
        if 'chart' in kwargs.keys():
            self.chart_type=kwargs['chart']
        if 'group' in kwargs.keys():
            self.group=kwargs['group']
        if 'queue_set' in kwargs.keys():
            self.queue_set=kwargs['queue_set']
        if 'rdb_address' in kwargs.keys():
            self.rethinkdb_address=kwargs['rdb_address']
        if 'rdb_dbname' in kwargs.keys():
            self.rethinkdb_dbname=kwargs['rdb_dbname']
        if 'queue_list' in kwargs.keys():
            self.queue_list = kwargs['queue_list']
        self.idg = 'gui'
        if 'id_gui' in kwargs.keys():
            self.idg = kwargs["id_gui"]
        if 'type_bus' in kwargs.keys():
                self.type_bus=kwargs['type_bus']

        self.nsta=len(self.group)
        self.data_queue=queue.Queue(maxsize=6)
        self.title='DragonCharts GNSS Real Time System'
        self.left=10
        self.top=10
        self.width=640
        self.height=480
        self.stations = {}
        self.assign_stations = {}
        self.process_group = {}
        self.position = {}
        self.guide = {}
        self.selected_sta=[]
        #Flag to control when the chart is avalaible to receive data
        self.chart_flag=False
        self.di=rdb.iso8601(get_datetime_di(delta=30))
        self.last_time={sta:self.di for sta in self.group}
        self.patrol=GatherSignal(self)
        print("WS x init ui")
        self.initUI()

    def connect_dbus(self):
        ############QDBUS CONFIGURARION
        #===========
        GatherDBusAdaptor(self)
        self.connection = QtDBus.QDBusConnection.sessionBus()
        #objectReg=self.connection.objectRegisteredAt(self.path)
        #self.connection.unregisterObject(self.path)
        #===============
        #regObj=self.connection.registerObject(self.path, self.interface, self)# register path, object
        #regSess=self.connection.registerService(self.service)# register service
        #call DBUS interface with my service, path, interface assigned to this object
        self.server=GatherDBusInterface(
                self.service,
                self.path,
                self.interface,
                self.connection,
                self)
        self.connection.connect(
                self.service,
                self.path,
                self.interface,
                self.variable,
                self.update_data)
        rprint(["service","path","interface","variable"])
        bprint([self.service, self.path, self.interface, self.variable])
        if not QtDBus.QDBusConnection.sessionBus().isConnected():#check if connected
            print("SS Bus no conectado")
            print(QtDBus.QDBusConnection.sessionBus().lastError().message())
        else:
            print("DBUS Connected to %s" %self.service)
        #connect interface message signal to slot
        #self.server.signal.connect(self.update_dataQ)#Get data from channel and -> print_data
        print("SS Signal asociada a print_data", flush=True)
        print("Interface")
        print(self.server)
        print(self.server.signal)
        #connect interface message signal to slot
        #self.server.signal.connect(self.update_dataQ)
        print("Server message %s" %self.server.signal )


    def initUI(self):
        bprint("Init GUI")
        self.main_widget=QWidget()
        # create button
        button=QPushButton("PyQT5 Button")
        button.setToolTip("Este es un botón de ejemplo")
        button.clicked.connect(self.on_click)
        # create button
        button_close=QPushButton("Cerrar")
        button_close.setToolTip("Cerrar Interfaz")
        button_close.clicked.connect(self.cerrar)
        #create textbox
        self.textbox = QLineEdit()
        self.textbox.resize(280,40)
        wn_options={}
        args=[QBoxLayout.TopToBottom]
        idl=self.create_layout(None, *args,  **wn_options)
        vbox = self.layouts[idl]['instance']
        vbox.addWidget(button_close)
        vbox.addWidget(button)
        vbox.addWidget(self.textbox)
        ts_args=[]
        ts_options={}
        idts=self.create_tabset(idl, *ts_args, **ts_options)
        #Tab for first chart:>
        t1_args=[QBoxLayout.LeftToRight]
        t1_options={'name':'Gráficos 1'}
        idt1=self.create_tab(idts, *t1_args, **t1_options)
        self.idt1=idt1
        #Tab for log and stations log:>
        t2_args=[QBoxLayout.LeftToRight]
        t2_options={'name':'Log'}
        idt2=self.create_tab(idts, *t2_args, **t2_options)
        #Tab for data log:>
        t3_args=[QBoxLayout.LeftToRight]
        t3_options={'name':'Data Log'}
        idt3=self.create_tab(idts, *t3_args, **t3_options)
        #Text for Log:>
        text_args=[]
        text_options={}
        idtext=self.create_text(idt2, *text_args, **text_options)
        #Text for data log:>
        text_args2=[]
        text_options2={}
        idtext2=self.create_text(idt3, *text_args2, **text_options2)
        self.log_text=idtext
        self.log_data=idtext2
        #  Set main window
        self.setCentralWidget(self.main_widget)
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage("Mensaje en Status Bar")
        #show
        self.connect_dbus()
        self.send_init()
        self.show()

    def send_init(self):
        q_send = self.queue_list[1]
        msg = {
            'command' : 'init_gui',
            'args':[self.idg, self.group]
        }
        print("Sending from GUI: %s" %msg)
        print("Queue : %s" %q_send)
        q_send.put(json.dumps(msg))
        msg={
            'command':'GET_STA',
            'args': []
        }
        rprint("INIT Request %s" %msg)
        q_send.put(json.dumps(msg))


    """
    Button actions
    """
    @pyqtSlot()
    def btn_create_window(self, **options):
        self.create_window(**options)

    @pyqtSlot()
    def on_click(self):
        print("PyQt5 button click")
        textboxValue=self.textbox.text()
        self.print_log(textboxValue)
        self.patrol.message({"msg":"Hola signal dbus %s" %textboxValue})
        """
        question=QMessageBox.question(self, "Mensaje de verificación","Usted escribió : %s ?" % textboxValue, QMessageBox.Ok, QMessageBox.No)
        if question == QMessageBox.Ok:
            print("Está ok")
        if question == QMessageBox.No:
            print("No fue")
        #Se vacía texto
        self.textbox.setText("")
        """

    @pyqtSlot()
    def cerrar(self):
        self.close()

    def add_process_queue(self, process_queue):
        self.process_queue = process_queue

    def add_gui_instance(self, code):
        #idd = self.get_id_by_code('DBDATA', code_db)
        # bprint("The db_data's id: %s" % idd)
        db_datos = dict(host='localhost',
                        port=6937,
                        name=None,
                        user=None,
                        passw=None,
                        code=code,
                        address=self.rethinkdb_address,
                        dbname=self.rethinkdb_dbname,
                        io_loop=self.loop,
                        env="gui_qt"
        )
        name='RethinkDB'
        try:
            print("WS Rethink data %s" %db_datos, flush=True)
            self.rethinkdb = RethinkDB(**db_datos)
            time.sleep(.5)
        except Exception as exec:
            print("Error al inicializar conexión %s" %exec)
            raise exec
        #self.common[code] = dict()
        return self.rethinkdb

    async def read_stations_queue(self):
        gprint("WS Read statuibs queue")
        print("WS====", flush=True)
        if not self.gui_queue.empty():
            #self.print_text("New Data from Datawork, count %s --->\n" %self.count)
            command=None
            gprint("WS Q size %s"%self.gui_queue.qsize())
            for i in range(self.gui_queue.qsize()):
                msg=self.gui_queue.get()
                gprint("WS msg %s" %msg)
                print("", flush=True)
                command=msg['command']
                data=msg['data']
                if command=='station':
                    self.stations.update(data)
                    station=tuple(data.items())
                    self.guide.update({station[0][1]['code']:station[0][0]})
                    station_ids=list(msg.keys())[0]
                    self.print_log(json.dumps(data))
                elif command=='position':
                    self.position.update(data)
                    self.print_log(json.dumps(data))
                elif command=='load_chart' and not self.chart_flag:
                    self.chart_flag=True
                    gprint("WS dd load chart")
                    self.init_chart()
                    time.sleep(1)
                #connect to rethinkdb
                #r=self.add_gui_instance("GUI")
                #conn=await self.connect_rethinkdb()
                #print("WS====", flush=True)

            #get data from rethinkdb
            dataset=await self.read_data()

            for sta,ds in dataset.items():
                self.load_data(sta,ds)

            self.gui_queue.task_done()
        print("", flush=True)


    def load_data(self,sta,ds):
        #Obtain from rethinkdb
        for msg in ds:
            gprint("WS msg %s" %msg, flush=True)
            command=msg['command']
            data=msg['data']
            rprint("WS dd station :> %s" %data['station_name'])
            bprint("WS dd Data process station :> %s" %data)
            bprint("WS dd Data command :> %s" %command)
            print("", flush=True)

            if command=='add_data':
                stn=None
                print("WS =================")
                if 'station_name' in data:
                    self.print_data(data['station_name'])
                    self.print_data(self.selected_sta)
                    stn=data['station_name']
                    print("QQ name :> %s" %stn)
                    print("QQ %s"%self.chart_flag)
                    print("QQ selected %s"%self.selected_sta)
                    print("QQ %s" %self.main_idchart)
                    print("QQ stn pertenece a selected: %s" %(stn in self.selected_sta))
                    print("", flush=True)

                    if self.chart_flag and stn in self.selected_sta:
                        self.print_data("QQ Add point to chart")
                        print("WTT Add point to chart %s -> %s" %(stn, data))
                        print("WTT =================")
                        self.data_queue.put(data)
                        if self.data_queue.full():
                            for i in range(self.data_queue.qsize()):
                                qdata = self.data_queue.get()
                                self.add_point(qdata)
                                self.data_queue.task_done()
                        self.renew_canvas()
                        #self.process_queue.task_done()
                    """
                    if self.chart_flag:
                        try:
                            self.print_data("Pre refresh traces")
                            self.renew_canvas()
                        except Exception as e:
                            self.print_data("Error en cargar plt %s" %e)
                            raise e
                    """
            print("", flush=True)

    async def connect_rethinkdb(self):
        r = self.rethinkdb
        conn=await r.async_connect()
        r.set_defaultdb(self.rethinkdb_dbname)
        await r.list_dbs()
        #await r.create_db(self.rethinkdb_dbname)
        await r.select_db(self.rethinkdb_dbname)
        #table_name=self.stations[ids]['db'] # created on datawork
        #gprint("Post station instance")
        rprint("Pre ending instance")
        await r.get_indexes(table_name)
        await r.list_tables(r.default_db)

    async def read_data(self):
        r=self.rethinkdb
        dataset={sta:[] for sta in self.group}
        #
        #self.last_time
        key='DT_GEN'
        for station in self.group:
            table_name="%s_GUI" %station
            try:
                di=self.last_time[station]
                filter_opt={'left_bound':'open', 'index':key}
                cursor=await r.get_data_filter(table_name,
                                               di,
                                               filter_opt,
                                               key)
                rprint("WW Data on %s result %s" %(code, cursor))
            except Exception as exec:
                print("Error en obtención de data desde rethinkdb")
                r.logerror("Error en la obtención de datos para estación %s en %s" %(code, di))
                await r.reconnect()
            if cursor:
                di=cursor[-1]['DT_GEN']
            self.last_time.update({station:di})
            dataset.update({station:cursor})
        return dataset

    def add_point(self, msg):
        try:
            gprint("WTT al añadir punto")
            print("WTT %s"%self.charts[self.main_idchart]['instance'])
            if self.main_idchart in self.charts.keys():
                chart=self.charts[self.main_idchart]['instance']
                chart.add_point(msg)
        except Exception as e:
            gprint("WTT Error al añadir punto")
            raise e

    def refresh_traces(self):
        pass

    def renew_canvas(self):
        print("QQT Chart keys  %s" %self.charts.keys())
        if self.main_idchart in self.charts.keys():
            chart=self.charts[self.main_idchart]['instance']
            print("QQT Chart  %s" %chart)
            chart.refresh_traces()
            print("WTT Num figs post")
            chart.draw()

    async def updates(self):
        print("WS on updates read stations", flush=True)
        count=self.count
        self.print_log("Ciclo de refresco nro: %s" %count)
        print("WS ==== SW", flush=True)
        #self.read_from_queue()
        #
        await self.read_stations_queue()
        await asyncio.sleep(1, loop=self.loop)
        count+=1
        self.count=count

    async def test(self):
        print("===", flush=True)
        print("WS Hola 1", flush=True)
        print("WS Hola 2", flush=True)
        print("WS Hola 3", flush=True)
        print("WS Hola 4", flush=True)
        await asyncio.sleep(2,loop=self.loop)
        #time.sleep(2)
        return "Ok"

    @pyqtSlot(list)
    def print_data(self, data):
        self.print_log("New data")
        self.print_log(json.dumps(data))


    @pyqtSlot(dict)
    def update_dataQ(self, new_data):
        self.print_data(new_data)

    @pyqtSlot(list)
    def update_data(self, new_data):
        msg=new_data[0]
        command=msg['command']
        data=msg['data']
        self.print_data(new_data)
        if command=='station':
            self.stations.update(data)
            station=tuple(data.items())
            self.guide.update({station[0][1]['code']:station[0][0]})
            station_ids=list(msg.keys())[0]
            self.print_log(json.dumps(data))
        elif command=='position':
            self.position.update(data)
            self.print_log(json.dumps(data))
        elif command=='load_chart' and not self.chart_flag:
            self.chart_flag=True
            gprint("WS dd load chart")
            self.init_chart()
            time.sleep(1)
        elif command=='source_groups':
            msg=data['msg']
            process_id=data['process']
            group=data['group']
            if msg=='update':
                self.process_group.update({"process":process_id, "group":group})
        elif command=='add_data':
            stn=None
            # check dict structure
            if 'station_name' in data:
                self.print_data(data['station_name'])
                #self.print_data(self.group)
                self.print_data(data)
                stn=data['station_name']
                if self.chart_flag and stn in self.group:
                    self.add_point(data)
                    self.renew_canvas()
        else:
            print("No command on msg")
            print(msg)

        print("", flush=True)

    def updates_task(self):
        #This function allows the system to call the coroutine that add a new_proces
        loop=self.loop
        asyncio.set_event_loop(loop)
        print("WS updates on updates task", flush=True)
        print("WS quamash loop is running? %s" % loop.is_running(), flush=True)
        try:
            args=[]
            bprint("WS New updates task on gui")
            print("", flush=True)
            task=loop.create_task(
                coromask(self.updates,
                         args,
                         simple_fargs))
            #
            print("WS result: %s" %task, flush=True)
            task.add_done_callback(
                functools.partial(
                    renew_quamash,
                    task,
                    self.updates,
                    simple_fargs,
                    loop
                )
            )
            gprint("WS task done added")

        except Exception as exec:
            rprint("Error en levantar corrutina de gui_check%s" %exec )
            raise exec
        if not loop.is_running():
            print("WS running new loop quamash", flush=True)
            loop.run_forever()


    def init_chart(self):
        #################DATA INPUT REF TO CHART
        # Is readed from database>
        # self.stations
        # self.position
        # self.guide
        ##############
        chart_args=[]
        #sl_stations=["VALN","QTAY","AEDA","ATJN"]
        #self.selected_sta=sl_stations
        #id_stations=[self.guide[s] for s in sl_stations]
        self.print_data(self.stations)

        #selection=['TRPD','ATJN','VALN','CRSC','RCSD','HSCO']

        selection=self.group
        
        keys_stations=[(key,s['code']) for (key, s) in self.stations.items() if s['code'] in selection]


        keys=[ k[0] for k in keys_stations ]
        stations=[ k[1] for k in keys_stations ]
        #self.selected_sta=stations
        print(stations)
        position=[(self.position[k]['llh']['lon'],self.position[k]['llh']['lat']) for k in keys]
        self.print_log("Estaciones a gráfico::::")
        self.print_log(json.dumps(stations))
        #define amount of stations
        n=self.nsta
        self.selected_sta=stations[:n]
        self.print_data(self.selected_sta)
        args=[self.selected_sta, position[:n]]
        chart_options={
            'widget':self.chart_type,
            'fnp':self.print_data,
            'display_window':600,
            'data_buffer':1200,
            'stations_per_canvas':n}
        gprint("RR Creating chart")
        idchart=self.create_chart(self.idt1, *args, **chart_options)
        self.main_idchart=idchart
        self.chart_flag=True
