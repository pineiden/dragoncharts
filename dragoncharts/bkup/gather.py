import asyncio
import functools

# NetworkTools methods and classes:
from networktools.dbs.redis import RedisData
from networktools.dbs.rethinkdb import RethinkDB
from networktools.dbs.geojson import GeoJSONData
from networktools.dbs.amqp import AMQPData


from networktools.library import my_random_string, check_gsof
from networktools.colorprint import gprint, bprint, rprint
from networktools.library import (pattern_value, fill_pattern, context_split,
                                  gns_loads, gns_dumps)
from networktools.geo import (radius, rad2deg, deg2rad,
                              ecef2llh, llh2ecef, get_from_ecef, ecef2neu)
from networktools.time import get_datetime_di
#RethinkDB module

import rethinkdb as rdb

# TaskTools for concurrency loop
from tasktools.taskloop import coromask, renew, simple_fargs, simple_fargs_out

# Async Socket
from gnsocket.gn_socket import GNCSocket

#Datetime
from datetime import tzinfo, timedelta, datetime

# Maths
import numpy as np
from networktools.library import geojson2rethinkjson
from networktools.library import geojson2json
from networktools.time import get_datetime_di
import time
from PyQt5.QtCore import QObject, pyqtSignal, QProcess
import simplejson as json
from multiprocessing import Lock

#garbage collector
import gc

class DragonGather():
    def __init__(self, queue_list, *args, **kwargs):
        # super().__init__()
        if 'ipt' in kwargs.keys():
            self.ipt = kwargs['ipt']
        if 'ico' in kwargs.keys():
            self.ico = kwargs['ico']
        if 'process_queue' in kwargs.keys():
            self.process_queue = kwargs['process_queue']
        if 'gui_queue' in kwargs.keys():
            self.gui_queue = kwargs['gui_queue']
        if 'proc_tasks' in kwargs.keys():
            self.proc_tasks = kwargs["proc_tasks"]
        if 'collector_address' in kwargs.keys():
            self.collector_address = kwargs["collector_address"]
        if 'stations' in kwargs.keys():
            self.stations = kwargs["stations"]
        if 'position' in kwargs.keys():
            self.position = kwargs["position"]
        if 'db_data' in kwargs.keys():
            self.db_data = kwargs["db_data"]
        if 'nproc' in kwargs.keys():
            self.lnproc = kwargs["nproc"]
        if 'sta_init' in kwargs.keys():
            print("K es sta init %s" % kwargs['sta_init'])
            self.sta_init = kwargs["sta_init"]
        if 'bridge' in kwargs.keys():
            self.bridge = kwargs["bridge"]
        if 'common' in kwargs.keys():
            self.common = kwargs["common"]
        if 'rdb_address' in kwargs.keys():
            self.rethinkdb_address = kwargs['rdb_address']
        if 'rdb_dbname' in kwargs.keys():
            self.rethinkdb_dbname = kwargs['rdb_dbname']
        if 'status_tasks' in kwargs.keys():
            self.sta_init = kwargs['status_tasks']
            self.status_tasks = kwargs['status_tasks']
        if 'assigned_tasks' in kwargs.keys():
            self.assigned_tasks = kwargs['assigned_tasks']
        if 'group' in kwargs.keys():
            self.group = kwargs['group']
        if 'gui_group' in kwargs.keys():
            self.gui_group = kwargs['gui_group']
        if 'gui_set' in kwargs.keys():
            self.gui_set = kwargs['gui_set']
        if 'signals' in kwargs.keys():
            self.signals = kwargs['signals']
        if 'isg' in kwargs.keys():
            self.isg = kwargs['isg']
        if 'sigid' in kwargs.keys():
            self.sigid = kwargs['sigid']
        if 'send_control' in kwargs.keys():
            self.sc = kwargs['send_control']
        if 'dbus_queue' in kwargs.keys():
            self.dbus_queue = kwargs['dbus_queue']
        if 'dbus_geo_queue' in kwargs.keys():
            self.dbus_geo_queue = kwargs['dbus_geo_queue']
        self.rq = queue_list[0]
        self.wq = queue_list[1]
        self.queue_list = queue_list
        self.start = 0
        self.signal = None
        self.origin_objects = {"RethinkDB": RethinkDB}
        self.rethinkdb = None
        self.rethinkdb_origin = {}
        self.lock = None
        self.msg_process = dict(
            GET_STA=self.get_sta
        )
        rprint("Dragon Gather init")

    def set_init(self, ids):
        print(type(self.sta_init))
        self.sta_init.update({ids: False})

    def set_ipt(self, uin):
        """
        Defines a new id for relation process-collect_task, check if exists
        """
        ipt = my_random_string(uin)
        while True:
            if ipt not in self.ipt:
                self.ipt.append(ipt)
                break
            else:
                ipt = my_random_string(uin)
        return ipt

    def set_ico(self, uin=4):
        """
        Defines a new id for task related to collect data insice a worker, check if exists
        """
        ico = my_random_string(uin)
        while True:
            if ico not in self.ico:
                self.ico.append(ico)
                break
            else:
                ico = my_random_string(uin)
        return ico

    def set_isg(self, uin=4):
        """
        Defines a new id for task related to collect data insice a worker, check if exists
        """
        isg = my_random_string(uin)
        while True:
            if isg not in self.isg:
                self.isg.append(isg)
                break
            else:
                isg = my_random_string(uin)
        return isg


    def msg_network_task(self):
        # get from queue status from SOCKET
        # send result
        # read_queue -> rq
        # process msg -> f(msg)
        gprint("Rescatando queues rq wq")
        queue_list=[self.rq, self.wq]
        self.gs = GNCSocket(mode='client')
        self.gs.set_address(self.collector_address)
        loop=asyncio.new_event_loop()
        # client mode to obtain idc
        gs = self.gs
        gs.set_loop(loop)
        idc = loop.run_until_complete(gs.create_client())
        self.load_data_task(loop, idc)
        gprint("XDX Gestionando mensajes en engine", flush=True)
        # gprint("XDX Gestionando mensajes en engine", flush=True)
        try:
            args = []
            # Create instances
            bprint("Inputs generic::::")
            rprint(args)
            bprint(self.check_status)
            task = loop.create_task(
                coromask(
                    self.check_status,
                    args,
                    simple_fargs_out)
            )
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    self.check_status,
                    simple_fargs_out)
            )
            if not loop.is_running():
                loop.run_forever()
        except Exception as exec:
            print("Error o exception que se levanta con %s" % format(queue_list))
            print(exec)
            raise exec

    async def check_status(self):
        rq = self.queue_list[0]
        wq = self.queue_list[1]
        process = dict()
        await asyncio.sleep(.5)
        try:
            msg_from_source = []
            # gprint("XDX rq test %s" %rq.empty(), flush=True)
            # gprint("XDX wq test %s" %wq.empty(), flush=True)
            if not rq.empty():
                for i in range(rq.qsize()):
                    msg = rq.get()
                    # print in blue
                    # gprint("XDX inside for %s" %msg, flush=True)
                    msg_from_source.append(msg)
            # rq.task_done()
            # process messages
            # bprint("XDX check new message %s" %msg_from_source, flush=True)
            for msg in msg_from_source:
                try:
                    # bprint("XDX pre dcode %s" %msg)
                    # bprint("XDX pre dcode type %s" %type(msg))
                    m = msg['dt']
                    idc = msg['idc']

                    mg=json.loads(m)
                    # print("XDX from Source::::>", flush=True)
                    # gprint("XDX %s" %msg, flush=True)
                    # rprint("XDX type %s" %type(msg), flush=True)
                    # print("XDX .", flush=True)
                    result=await self.interpreter(mg)
                    # gprint("XDX Respuesta", flush=True)
                    # bprint("XDX ",result)
                    if not None:
                        wq.put({'msg':result,'idc':idc})
                except Exception as exec:
                    print("Error al transformar")
                    print(exec)
                    print(msg)
                    print(type(msg))
                    raise exec
            # bprint(self.instances.keys())
        except Exception as exec:
            print(exec)
            raise exec
        return []


    async def interpreter(self, msg):
        # msg is a string JSON
        self.lock = Lock()
        command = msg['command']
        args = msg['args']
        result = None
        if command == 'init_gui':
            # if self.gui_group empty:
            if not self.gui_group:
                with self.lock:
                    self.sc.value = not self.sc.value
            # add group and id
            idg = args[0]
            group = args[1]
            # relationship between a gui and a group of stations
            self.gui_group.update({idg: group})
            
        if command in self.msg_process:
            bprint("Comando %s" %command)
            bprint("args %s" %args)
            result = self.msg_process[command](*args)
        return result

    def get_sta(self, *args):
        stations = self.stations
        # gprint("XDX stations keys : %s" %stations.keys())
        gprint("Stations:")
        [bprint(s) for s in stations.values()]
        print(self.dbus_queue)
        for ids in stations.keys():
            # bprint("XDX Cargando data a process")
            dataset = stations[ids]
            # rprint("XDX Cargando %s-> %s" %(ids, dataset))
            # gprint("XDX End dataset")
            # gprint("XDX Cargando group %s" %self.group)
            try:
                if dataset['code'] in self.group:
                    # rprint("XDX Put on process_queue %s" %ids)
                    if self.sc.value:
                        self.dbus_queue.put({
                            'command': 'station',
                            'data': {ids: self.stations[ids]}})
                        self.dbus_queue.put({
                            'command': 'position',
                            'data': {ids: self.position[ids]}})
                    self.gui_set.append(dataset['code'])
                    #print("XDX .sds", flush=True)

                # gprint(self.queue_process.qsize())
            except Exception as exc:
                print("Error al cargar ids a queue")
                raise exc
        self.dbus_queue.put({
            'command': 'load_chart',
            'data': []})
        return json.dumps({
            'command': 'GET_STA',
            'args': ['dbus']
        })


    def add_process_queue(self, process_queue):
        self.process_queue = process_queue

    def add_task(self, ids, ipt):
        self.proc_tasks[ipt] += [ids]

    async def recv_msg(self, loop, idc):
        ans = []
        gs = self.gs
        print(gs)
        msg = ''
        while not msg == '<END>':
            msg = await gs.recv_msg(idc)
            if msg != '<END>' and msg is not None:
                ans.append(msg)
            if msg == '<END>':
                break
        return ans

    async def receive_wc(self, idc):
        loop = asyncio.get_event_loop()
        msg = await self.recv_msg(loop, idc)
        for m in msg:
            print(m)
        return msg

    async def load_stations(self, idc):
        get_lst = "ADMIN|ADMIN|GET|LST|STA"
        gs = self.gs
        await gs.send_msg(get_lst, idc)
        loop = asyncio.get_event_loop()
        #print("Station loop")
        try:
            msg = await self.recv_msg(loop, idc)
        except Exception as exc:
            print(exc)
            raise exc
        #bprint("MSG recibido")
        qs = dict()
        POSITION = dict()
        for m in msg:
            print(m)
            msg_list = m.split('|')
            qs = gns_loads(msg_list[4])
            print(qs)
        for ids in qs:
            qs[ids]['STATUS'] = 'OFF'
            POSITION[ids] = dict()
            POSITION[ids]['ECEF'] = dict()
            print("KEYS %s" % POSITION.keys())
        for ids in qs:
            self.stations[ids] = qs[ids]
            if 'ECEF_Z' in qs[ids]:
                Z = qs[ids]['ECEF_Z']
                print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'Z': Z})
                gprint("Z: %s" % Z)
                gprint("Type %s" % type(POSITION[ids]['ECEF']))
                gprint("Z en position %s" % POSITION[ids]['ECEF'].keys())
            if 'ECEF_X' in qs[ids]:
                X = qs[ids]['ECEF_X']
                print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'X': X})
                gprint("X: %s" % X)
                gprint("Type %s" % type(POSITION[ids]['ECEF']))
                gprint("X en position %s" % POSITION[ids]['ECEF'].keys())
            if 'ECEF_Y' in qs[ids]:
                Y = qs[ids]['ECEF_Y']
                print(type(POSITION[ids]))
                POSITION[ids]['ECEF'].update({'Y': Y})
                gprint("Y: %s" % Y)
                gprint("Type %s" % type(POSITION[ids]['ECEF']))
                gprint("Y en position %s" % POSITION[ids]['ECEF'].keys())

            if 'position' in qs[ids]:
                pst = json.loads(qs[ids]['position'])
                coords = pst['coordinates']
                [lat, lon] = deg2rad(*coords)
                gprint("Station data:")
                bprint("Data from station %s :" % self.stations[ids]['code'])
                POSITION[ids].update({'lat': lat})
                POSITION[ids].update({'lon': lon})
                POSITION[ids].update({'radius': radius(lat)[0]})
                XYZ = llh2ecef(lat, lon, Z)
                # bprint(XYZ) ok, correct
                bprint(POSITION[ids].keys())
                POSITION[ids].update({'ECEF': dict(zip(['X', 'Y', 'Z'], XYZ))})
            x = POSITION[ids]['ECEF']['X']
            y = POSITION[ids]['ECEF']['Y']
            z = POSITION[ids]['ECEF']['Z']
            (lat, lon, h) = ecef2llh(x, y, z)
            POSITION[ids].update({'llh':{'lat': lat,'lon':lon,'z': h}})
            self.position[ids] = POSITION[ids]
            rprint("Position absoluta")
            rprint(self.position[ids].keys())
            if 'code' in qs:
                self.common[qs][ids] = dict()

    async def load_dbdata(self, idc):
        get_lst = "ADMIN|ADMIN|GET|LST|DB"
        gs = self.gs
        await gs.send_msg(get_lst, idc)
        loop = asyncio.get_event_loop()
        msg = await self.recv_msg(loop, idc)
        qs = dict()
        for m in msg:
            print(m)
            msg_list = m.split('|')
            qs = gns_loads(msg_list[4])
            print(qs)
        for k in qs:
            self.db_data[k] = qs[k]

    async def load_data(self, *args):
        """
        Load main data at the beggining
        In the future, must handle messages betwen
        DragonDataWork and Collector

        """
        loop = asyncio.get_event_loop()
        gs = self.gs
        idc = args[0]
        if self.start == 0:
            idc = await gs.create_client()
            await self.receive_wc(idc)
            await self.load_stations(idc)
            await self.load_dbdata(idc)
            stations = self.stations
            bprint(stations)
            #self.queue_list.put(stations)
            # send ids to queue
            await asyncio.sleep(3)
            print(self.stations)
            self.start = 1
            stations=self.stations
            for ids in stations.keys():
                #bprint("Cargando data a process")
                dataset=stations[ids]
                #rprint("Cargando %s-> %s" %(ids, dataset))
                #gprint("End dataset")
                #gprint("Cargando group %s" %self.group)
                try:
                    if dataset['code'] in self.group:
                        #rprint("Put on process_queue %s" %ids)
                        self.process_queue.put(ids)
                        self.gui_set.append(dataset['code'])

                    # gprint(self.queue_process.qsize())
                except Exception as exc:
                    print("Error al cargar ids a queue")
                    raise exc

        else:
            bprint("Wait sleep on fn load_data")
            await asyncio.sleep(25)

    def load_data_task(self, loop, idc):
        #bprint("Load data task")
        args = [idc]
        task = loop.create_task(coromask(self.load_data,
                                         args,
                                         simple_fargs))
        task.add_done_callback(functools.partial(renew,
                                                 task,
                                                 self.load_data,
                                                 simple_fargs))

    def add_sta_instance(self, ids, loop):
        # create bridge instace
        #bprint("Station %s and port %s" % (self.stations[ids]['code'], self.bridge))
        #gprint("Bridge: %s" % format(self.bridge))
        #local_port = self.create_bridge(ids)
        #rprint("Local port %s" %local_port)
        code = self.stations[ids]['code']
        code_db = self.stations[ids]['db']
        #idd = self.get_id_by_code('DBDATA', code_db)
        # bprint("The db_data's id: %s" % idd)
        db_datos = dict(host='localhost',
                        port=6937,
                        name=None,
                        user=None,
                        passw=None,
                        code=code_db,
                        address=self.rethinkdb_address,
                        dbname=self.rethinkdb_dbname,
                        io_loop=loop,
                        env='gather_%s' %code)
        name='RethinkDB'
        try:
            print(self.origin_objects)
            self.rethinkdb = self.origin_objects[name](**db_datos)
            time.sleep(.5)
        except Exception as exec:
            print("Error al inicializar conexión %s" %exec)
            raise exec
        #self.common[code] = dict()
        self.sta_init[ids]=True
        if name=='RethinkDB':
            self.rethinkdb_origin[ids]=True
        return self.rethinkdb


    # CREATE QUEUE INSTANCES


    # GET DATA AND SEND TO PLOT
    #
    #

    def add_process_instance(self, ids):
        CODE = self.stations[ids]['protocol'].upper()
        station = self.stations[ids]['code']
        kwargs = dict()
        kwargs['code'] = CODE
        kwargs['station'] = self.stations[ids]
        kwargs['position'] = self.position[ids]
        print(kwargs)
        process_instance=GeoJSONData(**kwargs)
        return process_instance


    def add_ew_instance(self, ids):
        ew_host='10.54.218.81'
        creds=('seismic','secret')
        CODE = self.stations[ids]['protocol'].upper()
        station = self.stations[ids]['code']
        kwargs = dict()
        kwargs['code'] = CODE
        kwargs['station'] = self.stations[ids]
        kwargs['position'] = self.position[ids]
        kwargs['host'] = ew_host
        kwargs['credentials']=creds
        print(kwargs)
        ew_instance=AMQPData(**kwargs)
        return ew_instance


    async def gather_data(self, ids, loop, sta):
        """
        This method it's maybe the most important because generate
        the instances and gather the data in a general loop

        The logging system is a task by process
        """
        ws=None
        await asyncio.sleep(.4)
        # bprint("Gather data on %s for %s" %(ids,sta))
        try:
            v = sta[1]
            if not self.sta_init[ids]:
                code = self.stations[ids]['code']
                code_db = self.stations[ids]['db']
                #gprint("Pre station instance to RethinkDB")
                r = self.add_sta_instance(ids, loop)
                conn=await r.async_connect()
                r.set_defaultdb(self.rethinkdb_dbname)
                await r.list_dbs()
                await r.select_db(self.rethinkdb_dbname)
                table_name=self.stations[ids]['db'] # created on datawork
                #rprint("Pre ending instance")
                await r.get_indexes(table_name)
                await r.list_tables(r.default_db)
                #
                control=None
                di=rdb.iso8601(get_datetime_di(delta=30))
                process_data=self.add_process_instance(ids)
                ew=self.add_ew_instance(ids)
                wargs=[ids, loop, [di, control, r, process_data, ew]]
                bprint("To next iteration:::::::----->")
                #gprint("Come back to define:::: %s" % wargs)
                return wargs

            elif self.sta_init[ids]:
                # signal.message({"msg":"There are data here XXX?"})
                # rprint("Ingresando a obtencion de data")
                # rprint(sta)
                di=sta[0]
                control=sta[1]
                r=sta[2]
                process_data=sta[3]
                ew=sta[4]
                code = self.stations[ids]['code']
                table_name = self.stations[ids]['db']
                key='DT_GEN'
                # bprint("DATETIME:::%s" %di, flush=True)
                # bprint("Table %s"%table_name)
                #signal.message({"msg":"Table %s"%table_name})
                try:
                    df = rdb.iso8601(get_datetime_di(delta=0)) #now
                    filter_opt = {'left_bound':'open', 'index':key}
                    cursor = await r.get_data_filter(table_name,
                                                   [di, df],
                                                   filter_opt,
                                                   key)
                    #rprint("WW Data on %s result %s" %(code, cursor))
                except Exception as exec:
                    print("Error en obtención de data desde rethinkdb")
                    r.logerror("Error en la obtención de datos para estación %s en %s" %(code, di))
                    await r.reconnect()

                # signal.message({"msg":"cursor len %s" %len(cursor)})
                list_ok=[]
                for c in cursor:
                    if check_gsof(c):
                        try:
                            result=process_data.manage_data(c)
                            #send data to ew
                            ew.manage_data(result)
                            #to gui
                            to_plot=geojson2json(result)
                            #bprint("WW %s to gui queue data -> %s" %(code, to_plot))
                            #signal.message({"msg":"Receive the data"})
                            send={'command':'add_data', 'data':json.loads(json.dumps(to_plot))}
                            geo_send={'geojson':json.loads(json.dumps(result))}
                            if self.sc.value:
                                # gprint("Sending by dbus %s data -> %s" %(self.dbus_queue, send))
                                self.dbus_queue.put(send)
                                #bprint("Keys in data %s" %c.keys())
                                if 'BATT_MEM' in c.keys():
                                    send={'command':'add_status','station':code ,'data':json.loads(json.dumps(c['BATT_MEM']))}
                                    #gprint("Enviando status %s" %send)
                                    self.dbus_queue.put(send)
                        except Exception as exec:
                            print("WS Error al enviar %s a cola %s" %(code, exec), flush=True)
                            raise exec
                        list_ok.append(c)
                    else:
                        msg = "Error en estacion %s  ----> %s" %(code, c)
                        bprint(msg, flush=True)
                        await r.msg_log(msg, "DEBUG")           
                if cursor:
                    if 'DT_GEN' in cursor[-1]:
                        di=cursor[-1]['DT_GEN']
                    elif 'DT_RECV' in cursor[-1]:
                        di=cursor[-1]['DT_GEN']
                    control=True
                else:
                    di=di
                # NO need connection
                # Gather data:
                return [ids, loop, [di, control, r, process_data, ew]]
        except Exception as exec:
            raise exec

        return [ids, loop, sta]


    async def status_gather_task(self, ids, loop, wargs_list):
        # List of stations asigned to cpu
        # await asyncio.sleep(5)

        # bprint("Wargs dict on status gather task %s" % wargs_list)
        # Generate gather data over the list on same cpu

        task_result={}

        # bprint(self.status_tasks)
        # bprint("%s : %s" % (ids, self.status_tasks[ids]))
        if self.status_tasks[ids]:
            # bprint(self.stations)
            try:
                #bprint("Wargs dict on status gather task %s" % wargs_list)
                wargs=[ids, loop, wargs_list]
                #bprint("Ids %s WARGS %s" %(self.stations[ids]['code'], wargs))
                #rprint("Gather Data to %s" %self.stations[ids]['code'])
                result = await self.gather_data(*wargs)

                """
                task.add_done_callback(functools.partial(renew,
                                                         task,
                                                         self.gather_data,
                                                         simple_fargs_out))
                
                """
                # bprint("HIPER RESULT::::::::::::: %s" %result)
                task_result.update({ids:result[2]})
                wargs = result[2]
                wargs_list=wargs
                # gprint(ids)
                self.status_tasks[ids]=True
            except Exception as exec:
                raise exec
        #await asyncio.sleep(.4)
        # gprint("Wargs list out: %s" % wargs_list)
        #collect garbage
        #gc.collect()
        return [ids, loop, wargs_list]


    async def gather_process(self, ipt, loop, ipt_wargs_dict):
        result_dict={}
        ids_list=self.proc_tasks[ipt]
        for ids in ids_list:
            if not ids in ipt_wargs_dict.keys():
                wargs = [None,None,None,None]
                ipt_wargs_dict.update({ids:wargs})
            wargs_dict=[ids, loop, ipt_wargs_dict[ids]]
            result=await self.status_gather_task(*wargs_dict)
            result_dict.update({ids:result[2]})
        return [ipt, loop, result_dict]


    async def process_sta_task(self, ipt, ico, *args):
        # Process gather data for ids station
        # rprint("Generating PROCESS STA TASK")
        # bprint("Tasks avalaibles %s" %self.assigned_tasks[ipt] )
        assigned_task=None
        if ico in self.assigned_tasks[ipt].keys():
            assigned_task=self.assigned_tasks[ipt][ico]#That is an ids code
        result=None
        if assigned_task:
            # change
            # print("ST Station %s assigned to task %s on process %s" %(assigned_task, ico, ipt), flush=True)
            # print("ST args", flush=True)
            # print(args)
            if not self.sta_init[assigned_task]:
                args=[assigned_task, args[1], args[2]]
                # print("ST ARGS to gather...._> %s" %args, flush=True)
            result=await self.gather_data(*args)
        else:
            #Every 5 secs check if there are new station to add
            #gprint("Process data avoid \n")
            await asyncio.sleep(1)
            result=args
        return [ipt, ico, *result]

    async def process_sta_manager(self,count, ipt):
        # Manage asignation of station to task inside ipt process
        ids_list=self.proc_tasks[ipt]
        ass_tasks=self.assigned_tasks[ipt]
        #gprint("IDS LIST::::::: MANAGER::::%s" %ids_list)
        for ids in ids_list:
            #gprint("ids %s on assg tasks  %s" %(ids, self.assigned_tasks[ipt].values()))
            if not ids in self.assigned_tasks[ipt].values():
                #bprint("Keys on ass tasks %s" %self.assigned_tasks[ipt].keys())
                for ico in self.assigned_tasks[ipt].keys():
                    value=self.assigned_tasks[ipt][ico]
                    if value is None:
                        # rprint("In process %s, task %s,  station %s" %(ipt,ico, ids))
                        ass_tasks.update({ico:ids})
                        break
        self.assigned_tasks[ipt]=ass_tasks
            #ids, loop, sta

        if count==0:#every 5min
            if self.sc.value:
                self.dbus_queue.put({
                    'command':'source_groups',
                    'data':{
                        'msg':'update',
                        'process':ipt,
                        'group':self.proc_tasks[ipt]}})
            count +=1
        elif count<300:
            count +=1
        elif count==300:
            count=0
        await asyncio.sleep(1)
        return count, ipt

    def gather_task(self,ipt):
        #loop=asyncio.get_event_loop()
        loop=asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        # print("WS Gather data ok for %s" %ipt)

        v=1
        tasks=[]
        # bprint("Collect task ipt: %s" %ipt)
        try:
            #create signal manager <- GatherSignal
            #
            self.assigned_tasks[ipt]={}
            new_dict={}
            for i in range(self.lnproc):
                ico=self.set_ico()
                nd={ico:None}
                #bprint("Ico generated %s" %nd)
                new_dict.update(nd)
            #rprint("New dict %s" %new_dict)
            self.assigned_tasks[ipt]=new_dict

            # rprint("Assigned tasks: %s"%self.assigned_tasks[ipt])

            for ico in self.assigned_tasks[ipt]:#Over limited tasks on process<=lnproc
                stax=[None,None,None,None]
                sargs=[None, loop, stax]
                # gprint("Code process: %s" %ipt)
                args=[ipt, ico, *sargs]
                task=loop.create_task(
                    coromask(
                    self.process_sta_task,
                    args,
                    simple_fargs_out)
                )
                task.add_done_callback(
                    functools.partial(renew,
                                      task,
                                      self.process_sta_task,
                                      simple_fargs_out)
                )
                tasks.append(task)
        except Exception as exec:
            print("Error en collect_task, gather stations %s, error %s" %(ipt, exec))
            print(exec)
            raise exec

        #Task manager
        try:
            count=0
            args=[count, ipt]
            task=loop.create_task(
                coromask(
                self.process_sta_manager,
                args,
                    simple_fargs_out)
            )
            task.add_done_callback(
                functools.partial(renew,
                                  task,
                                  self.process_sta_manager,
                                  simple_fargs_out)
            )
            tasks.append(task)
        except Exception as exec:
            print("Error en collect_task, manager %s, error %s" %(ipt, exec))
            print(exec)
            raise exec
        if not loop.is_running():
            #loop.run_until_complete(asyncio.gahter(*tasks))
            loop.run_forever()
