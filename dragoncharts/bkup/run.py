import asyncio
import concurrent.futures
import time
import functools
import math
import sys
from ctypes import c_bool
from tasktools.taskloop import coromask, renew, simple_fargs
from multiprocessing import Manager, Queue, Lock
from multiprocessing.managers import BaseManager, SyncManager
from networktools.colorprint import gprint, bprint, rprint
from networktools.environment import get_env_variable
from networktools.ports import used_ports, get_port
from networktools.ssh import bridge, kill

from settings import GUI_STATIONS_BY_WORKER, GUI_WORKERS,GROUP, st_dict
from settings import host, gnsocket_port, user, nproc
#from interface import DragonInterface
from gather import DragonGather
from PyQt5.QtWidgets import QApplication
from qtgui.interface import DragonInterface

import numpy as np

import argparse

from quamash import QEventLoop, QThreadExecutor

from PyQt5.QtCore import QObject, pyqtSignal
from qtgui.dbus import GatherSignal

# Enables a socket to listen messages and send some data
from gnsocket.gn_socket import GNCSocket

from networktools.library import pattern_value, \
    fill_pattern, context_split, \
    gns_loads, gns_dumps



AEventLoop = type(asyncio.get_event_loop())
class QEventLoopPlus(QEventLoop, AEventLoop):
    pass

from PyQt5.QtCore import QObject, pyqtSignal, QProcess


parser=argparse.ArgumentParser(description="Obtener parámetros de operación")

parser.add_argument('--group',
                    help="Select the group of stations {0,1,ALL}",)
parser.add_argument('--workers',
                    type=int,
                    help="Select amount of workers to run with")
parser.add_argument('--eXw',
                    type=int,
                    help="Amount of stations by workers, to get data and process")

if __name__ == "__main__":
    args = parser.parse_args()
    workers = nproc
    if args.workers:
        workers = args.workers
    est_by_proc = GUI_STATIONS_BY_WORKER
    group = GROUP
    if args.group:
        group = st_dict[args.group]
    if args.eXw:
        est_by_proc = args.eXw

    dt_status = "GROUP"
    dt_group = group

    print(args)
    # create a bridge to collector system, to connect GNCSocket
    address = ('localhost', 6666)
    gs = GNCSocket(mode='server')
    gs.set_address(address)

    tsleep = 1

    # GNCSocket port
    port = gnsocket_port
    up = used_ports()
    [local_port, up] = get_port(up)
    address = ('localhost', local_port)

    # Bridge to datawork redis
    ssh_bridge = bridge(local_port, port, host, user)

    dt_status = "ALL"

    # amount of chars for process identificators
    uin_p = 4
    nproc = nproc
    loop = asyncio.get_event_loop()
    executor = concurrent.futures.ProcessPoolExecutor(workers)
    manager = Manager()
    sta_assigned = manager.list()
    ipt = manager.list()
    ico = manager.list()
    queue_n2t = manager.Queue()
    # network->terminal
    queue_t2n = manager.Queue()
    queue_list = [queue_n2t, queue_t2n]
    # terminal->network
    process_queue = manager.Queue()
    stations_queue = manager.Queue()
    tasks_queue = manager.Queue()
    ans_queue = manager.Queue()
    dbus_geo_queue = manager.Queue()
    dbus_queue = manager.Queue()
    proc_tasks = manager.dict()
    assigned_tasks = manager.dict()
    stations = manager.dict()
    position = manager.dict()
    db_data = manager.dict()
    sta_init = manager.dict()
    gui_group = manager.dict()

    print("Tipo.:::::%s" %type(sta_init))
    kwargs=dict()
    status_tasks=manager.dict()

    # list of process identificators
    proc_tasks=manager.dict()
    signals=manager.dict()

    # create gui queue

    gui_queue=manager.Queue()

    gui_set=manager.list()

    # QT Signal in shared memory

    async def sock_write(queue_t2n, idc):
        queue=queue_t2n
        await asyncio.sleep(tsleep)
        try:
            #rprint("XDX Check queue from source", flush=True)
            #print("XDX queue empty ", queue.empty())
            if not queue.empty():
                for i in range(queue.qsize()):
                    msg_in = queue.get()
                    msg = msg_in['msg']
                    idc = msg_in['idc']
                    #print("XDX En viando mensaje: %s" %msg_in, flush=True)
                    await gs.send_msg(gns_dumps(msg), idc)
                await gs.send_msg("<END>", idc)
            else:
                pass
        except Exception as exec:
            print("Error con modulo de escritura del socket")
            raise exec

    # socket communication terminal to engine
    async def sock_read(queue_n2t, idc):
        queue=queue_n2t
        msg_from_engine=[]
        await asyncio.sleep(tsleep)
        try:
            # bprint("XDX Check socket from gui", flush = True)
            # read queue is answer from msg sended
            datagram = await gs.recv_msg(idc)
            bprint("XDX Mensaje recibido: %s" %datagram, flush=True)
            bprint("XDX datagram: %s" % datagram)
            if not datagram == '' and \
               datagram != "<END>":
                queue.put(
                    {'dt': gns_loads(datagram),
                     'idc': idc}
                )
        except Exception as exec:
            raise exec

    def socket_task():
        print("XDX socket loop inside", flush=True)
        loop = asyncio.get_event_loop()
        gs.set_loop(loop)

        async def socket_io(reader, writer):
            queue_read = queue_list[0]
            queue_write = queue_list[1]
            idc = await gs.set_reader_writer(reader, writer)
            # First time welcome
            # welcome="Welcome to socket"
            # rprint(welcome)
            # await gs.send_msg(welcome)
            # await gs.send_msg("<END>")
            # task reader
            try:
                args = [queue_read, idc]
                task = loop.create_task(
                    coromask(
                        sock_read,
                        args,
                        simple_fargs)
                )
                task.add_done_callback(
                    functools.partial(
                        renew,
                        task,
                        sock_read,
                        simple_fargs)
                )
                args = [queue_write, idc]
                # task write
                task = loop.create_task(
                    coromask(
                        sock_write,
                        args,
                        simple_fargs)
                )
                task.add_done_callback(
                    functools.partial(
                        renew,
                        task,
                        sock_write,
                        simple_fargs)
                )
            except Exception as exec:
                raise exec

        ########
        # Insert a coroutine with reader and writer tasks
        future = loop.create_task(gs.create_server(socket_io))
        if not loop.is_running():
            loop.run_forever()

    ##

    gprint(address)
    kwargs.update({'ipt': ipt})
    kwargs.update({'ico': ico})
    kwargs.update({'collector_address': address})
    kwargs.update({'stations': stations})
    kwargs.update({'position': position})
    kwargs.update({'db_data': db_data})
    kwargs.update({'proc_tasks': proc_tasks})
    kwargs.update({'nproc': nproc})
    kwargs.update({'sta_init': sta_init})
    kwargs.update({'bridge': bridge})
    kwargs.update({'status_tasks': status_tasks})
    kwargs.update({'assigned_tasks': assigned_tasks})
    kwargs.update({'rdb_address':('atlas.csn.uchile.cl',28015)})
    kwargs.update({'rdb_dbname': 'collector'})
    kwargs.update({'chart': 'gnss_graph'})
    kwargs.update({'gui_set': gui_set})
    kwargs.update({'gui_queue': gui_queue})
    kwargs.update({'process_queue': process_queue})
    kwargs.update({'signals': signals})
    kwargs.update({'isg': manager.list()})
    kwargs.update({'sigid': manager.list()})
    kwargs.update({'group': dt_group})#Specifi group
    kwargs.update({'gui_group': gui_group})#gui_group
    kwargs.update({'dbus_queue': dbus_queue})
    kwargs.update({'dbus_geo_queue': dbus_geo_queue})
    kwargs.update({'send_control': manager.Value(c_bool, False)})
    kwargs.update({'type_bus': 'session'})

    gather=DragonGather(queue_list, **kwargs)
        
    async def new_process(queue_tasks):
        await asyncio.sleep(10)
        #rprint("New Process")
        #bprint("----")
        msg_in=[]
        try:
            tasks=[]
            W=0
            if not queue_tasks.empty():                
                for i in range(queue_tasks.qsize()):
                    ids=queue_tasks.get()
                    #bprint(ids)
                    #gprint(gather.stations.keys())
                    #rprint(gather.lnproc)
                    gather.status_tasks[ids]=True
                    gather.sta_init[ids]=True
                    #bprint(len(gather.proc_tasks))
                    #gprint(sta_assigned)
                    #rprint("IDS %s is in list %s" % (ids, gather.stations.keys()))
                    if ids in gather.stations.keys():
                        q=0
                        #bprint("IPT tasks...::::_>>>>>>%s" %gather.proc_tasks)
                        for ipt in gather.proc_tasks.keys():
                            #gprint("Tasks on processor %s are : %s" % (ipt,gather.proc_tasks[ipt]))
                            q+=1
                            if len(gather.proc_tasks[ipt])<gather.lnproc and \
                               not ids in sta_assigned:
                                if dt_status == 'GROUP':
                                    if gather.stations[ids]['code'] in dt_group  :
                                        #rprint("Asignando tarea %s a procesador %s" %(ids,ipt))
                                        gather.add_task(ids,ipt)
                                        #bprint("Tasks on processor %s" % gather.proc_tasks )
                                        gather.set_init(ids)
                                        sta_assigned.append(ids)
                                        ans="TASK %s ADDED TO %s" % (ids, ipt)
                                        #gprint(ans)
                                elif dt_status == 'ALL':
                                    #rprint("Asignando tarea %s a procesador %s" %(ids,ipt))
                                    gather.add_task(ids,ipt)
                                    #bprint("Tasks on processor %s" % gather.proc_tasks )
                                    gather.set_init(ids)
                                    sta_assigned.append(ids)
                                    ans="TASK %s ADDED TO %s" % (ids, ipt)
                                    #gprint(ans)


                queue_tasks.task_done()
        except Exception as exec:
            bprint("Error en asignación de tareas a procesador: %s" %exec)
            raise exec

    def new_process_task(queue_tasks):
        #task

        """
        This function allows the system to call the coroutine that add a new_process
        """
        #loop = asyncio.get_event_loop()
        loop=asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        try:
            args=[queue_tasks]
            bprint("New process task")
            task=loop.create_task(
                coromask(
                    new_process,
                    args,
                    simple_fargs)
            )
            #
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    new_process,
                    simple_fargs)
            )
        except Exception as exec:
            rprint("Error en levantar corrutina %s" %exec )
            raise exec
        if not loop.is_running():
            loop.run_forever()


    async def signal_send(queue, signal):
        if not queue.empty():
            for i in range(queue.qsize()):
                msg = queue.get()
                #rprint("MSG FROM DATAWORK %s" %msg)
                #bprint(signal)
                #gprint("Isconnected %s" %signal.connection.isConnected())
                #bprint("Error %s" % signal.connection.lastError().message())
                #rprint("Service %s" % signal.connection.baseService())
                signal.connectdbus()
                if type(msg) == dict:
                    #bprint("Enviando msg %s" %msg)
                    signal.message(msg)
                queue.task_done()
        await asyncio.sleep(.5)
        return [queue, signal]

    def signal_task(queue):
        bprint("Signal Task", flush=True)
        loop=asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        signal=GatherSignal()
        #signal.message({'msg':"Hello %s" %10})

        try:
            args=[queue, signal]
            bprint("New process task")
            task=loop.create_task(
                coromask(
                    signal_send,
                    args,
                    simple_fargs)
            )
            #
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    signal_send,
                    simple_fargs)
            )
        except Exception as exec:
            rprint("Error en levantar corrutina %s" %exec )
            raise exec
        if not loop.is_running():
            loop.run_forever()


    ## GEOJSON SIGNAL


    async def geojson_signal_send(queue, signal):
        if not queue.empty():
            for i in range(queue.qsize()):
                msg = queue.get()
                #rprint("MSG FROM DATAWORK %s" %msg)
                #bprint(signal)
                #gprint("Isconnected %s" %signal.connection.isConnected())
                #bprint("Error %s" % signal.connection.lastError().message())
                #rprint("Service %s" % signal.connection.baseService())
                signal.connectdbus()
                if type(msg) == dict:
                    #bprint("Enviando msg %s" %msg)
                    signal.message(msg)
                queue.task_done()
        await asyncio.sleep(.5)
        return [queue, signal]

    def geojson_signal_task(queue):
        bprint("Geojson Signal Task", flush=True)
        loop=asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        service = 'csn.atlas.geojson'
        interface = "%s.interface" %service
        kwargs = {
            'service':service,
            'interface':interface            
        }
        signal=GatherSignal(**kwargs)
        #signal.message({'msg':"Hello %s" %10})

        try:
            args=[queue, signal]
            bprint("New process task")
            task=loop.create_task(
                coromask(
                    geojson_signal_send,
                    args,
                    simple_fargs)
            )
            #
            task.add_done_callback(
                functools.partial(
                    renew,
                    task,
                    geojson_signal_send,
                    simple_fargs)
            )
        except Exception as exec:
            rprint("Error en levantar corrutina %s" %exec )
            raise exec
        if not loop.is_running():
            loop.run_forever()



    tasks_list=[]

    # signal task

    # second process -> tasks asignment

    signaltask=loop.run_in_executor(
        executor,
        functools.partial(
            signal_task,
            dbus_queue))

    tasks_list.append(signaltask)
    
    geosignaltask=loop.run_in_executor(
        executor,
        functools.partial(
            geojson_signal_task,
            dbus_geo_queue))

    tasks_list.append(signaltask)

    # second process -> tasks asignment
    process_task=loop.run_in_executor(
        executor,
        functools.partial(
            new_process_task,
            process_queue))

    tasks_list.append(process_task)



    # Task on gather to process messages RPC
    # Active client SOCKET connected with Collector

    networktask = loop.run_in_executor(
        executor,
        gather.msg_network_task
    )
    tasks_list.append(networktask)



    # Task to receive on socket messages from outside


    # SOCKET SERVER TASK
    sockettask=loop.run_in_executor(
        executor,
        socket_task
    )
    tasks_list.append(sockettask)
    """

    socket_task ---> networktask
    A ---> B
    A sends to B using queues a message

    """

    NIPT=workers-4#Workers avalaible

    for i in range(NIPT):
        #time.sleep(1)
        ipt=gather.set_ipt(4)
        gather.proc_tasks[ipt]=[]
        gprint("Se inicia proceso %s" % ipt)
        bprint("Proc task...%s"%gather.proc_tasks)
        # problem here for python v3.6>
        task_process=loop.run_in_executor(
            executor,
            functools.partial(
                gather.gather_task,
                ipt))
        tasks_list.append(task_process)



    loop.run_until_complete(asyncio.gather(*tasks_list))

